import { Mongo } from 'meteor/mongo';

export const ProjectCategories = new Mongo.Collection('projectCategories');