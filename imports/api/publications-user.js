import { Meteor } from 'meteor/meteor';

Meteor.publish('getUserWithRoles', function () {
  if (!this.userId) this.error(new Meteor.Error('Not authorized'));
  const publicFields = { roles: 1 };
  return Meteor.users.find(this.userId, { fields: publicFields });
});
