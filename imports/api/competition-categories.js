import { Mongo } from 'meteor/mongo';

export const CompetitionCategories = new Mongo.Collection('competitionCategories');