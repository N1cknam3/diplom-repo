import firebase from 'firebase/app';
import 'firebase/storage';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCDuzwWdXq4ip4Rm2YtlJKcBI5c7FxCxOY",
  authDomain: "diplom-irina.firebaseapp.com",
  databaseURL: "https://diplom-irina.firebaseio.com",
  projectId: "diplom-irina",
  storageBucket: "diplom-irina.appspot.com",
  messagingSenderId: "796727496674",
  appId: "1:796727496674:web:cbefcf337ae629fb"
};
firebase.initializeApp(firebaseConfig);

const storageImageRef = firebase.storage().ref('images');
const storageMethodSpaceMaterialsRef = firebase.storage().ref('files/method-space');
const storageCompetitionApplicationsRef = firebase.storage().ref('files/competitions');
const storageProjectApplicationsRef = firebase.storage().ref('files/projects');

export {
  storageImageRef,
  storageMethodSpaceMaterialsRef,
  storageCompetitionApplicationsRef,
  storageProjectApplicationsRef,
  firebase as default
}