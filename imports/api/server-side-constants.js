export default {
  AutoUsers: [
    { email: 'admin@example.com', password: 'qwerty', roles: ['admin'] },
    { email: 'methodist@example.com', password: 'qwerty', roles: ['methodist'] },
  ]
}