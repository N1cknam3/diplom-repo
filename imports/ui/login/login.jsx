import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import withStyles from '@material-ui/core/styles/withStyles';
import { withRouter } from 'react-router';

import AppSnackBar from '/imports/ui/components/AppSnackBar';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ClientConstants from '/imports/api/client-side-constants';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});

export class Login extends Component {

  state = {
    documentTitle: 'Авторизация',
    snackBar : {
      open: false,
      message: '',
      variant: 'info'
    },
    email: '',
    password: '',
    formErrors: {email: '', password: ''},
    emailValid: false,
    passwordValid: false,
    formValid: false
  };

  componentDidMount() {
    document.title = this.state.documentTitle;
  }

  submitForm = (event) => {
    event.preventDefault();
    const email = this.state.email;
    const password = this.state.password;
    Meteor.loginWithPassword(email, password, (err) => {
      if (err) {
        this.setState({snackBar : {open: true, message: ClientConstants.formCheckEmailPassword, variant: 'error'}});
      } else {
        this.props.history.push('/methodist');
      }
    });
  };

  handleFormInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value});
  };

  closeSnackBar = (event, reason) => {
    if (reason === 'clickaway') return;
    this.setState({snackBar : {...this.state.snackBar, open: false, variant: 'info'}});
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.main}>
        <CssBaseline />
        <AppSnackBar
          open={this.state.snackBar.open}
          onClose={this.closeSnackBar}
          message={this.state.snackBar.message}
          variant={this.state.snackBar.variant}
        />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            {this.state.documentTitle}
          </Typography>
          <form className={classes.form} onSubmit={this.submitForm}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Email адрес</InputLabel>
              <Input
                id="email"
                name="email"
                type="email"
                value={this.state.email}
                onChange={this.handleFormInput}
                autoComplete="email"
                autoFocus />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Пароль</InputLabel>
              <Input
                id="password"
                name="password"
                type="password"
                value={this.state.password}
                onChange={this.handleFormInput}
                autoComplete="current-password" />
            </FormControl>
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Запомнить"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Войти
            </Button>
          </form>
        </Paper>
      </div>
    )
  }
}

export default withRouter(withStyles(styles)(Login))