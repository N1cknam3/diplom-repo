import React, { Component } from 'react';
import { Link, Route, Switch } from "react-router-dom";
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import { _ } from 'underscore';
import AppBar from '@material-ui/core/AppBar';
import CompetitionsSection from '/imports/ui/methodist/competitions/competitions-section';
import DefaultWrapper from '/imports/ui/components/DefaultWrapper';
import MethodSpacesSection from '/imports/ui/methodist/method-space/method-spaces-section';
import NewsSection from '/imports/ui/methodist/news/news-section';
import Paper from '@material-ui/core/Paper';
import ProjectsSection from '/imports/ui/methodist/projects/projects-section';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  tabs: {
    marginBottom: 20
  },
  tabButton: {
    transition: 'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,' +
                'box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,' +
                'border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    '&:hover': {
      backgroundColor: 'rgba(255,255,255, .15)'
    }
  }
});


tabNames = [
  { 
    path: 'news',
    routePath: '/:itemId?',
    text: 'Новости',
    render: (props, baseUrl) => <NewsSection {...props} baseUrl={baseUrl}/>
  },
  { 
    path: 'method-space',
    routePath: '/:categoryId?/:itemId?',
    text: 'Методическое пространство',
    render: (props, baseUrl) => <MethodSpacesSection {...props} baseUrl={baseUrl}/>
  },
  { 
    path: 'competitions',
    routePath: '/:categoryId?/:itemId?',
    text: 'Конкурсы',
    render: (props, baseUrl) => <CompetitionsSection {...props} baseUrl={baseUrl}/>
  },
  { 
    path: 'projects',
    routePath: '/:categoryId?/:itemId?',
    text: 'Проекты',
    render: (props, baseUrl) => <ProjectsSection {...props} baseUrl={baseUrl}/>
  }
];


class Methodist extends Component {

  state = {
    documentTitle: 'Кабинет методиста',
    selectedTab: tabNames[0].path,
  }


  componentDidMount() {
    document.title = this.state.documentTitle;
    const { path } = this.props.match.params;
    if (path) this.setState({ selectedTab: path });
  }


  tabChange = value => {
    this.setState({ selectedTab: value });
  }


  renderTabs = () => {
    const { classes } = this.props;

    return _.map(tabNames, ({ path, text }) =>
      <Tab
        key={ path }
        className={ classes.tabButton }
        label={ text }
        component={ Link }
        to={ "/methodist/" + path }
        value={ path } />
    )
  }


  renderRedirects = () => {
    return (
      <Switch>
        {_.map(tabNames, ({ path, routePath, render }) => {
            const baseUrl = this.props.match.url;
            return (
              <Route
                key={ path }
                path={ "/methodist/" + path + routePath }
                // component={ component } 
                render={ (props) => render(props, baseUrl) }/>
            )
          }
        )}
      </Switch>
    )
  }


  render() {
    const { classes } = this.props;
    const { selectedTab } = this.state;

    return (
      <DefaultWrapper title={this.state.documentTitle}>
        <Paper className={classes.root} elevation={1}>
          <Typography variant="h5" component="h3">
            Добро пожаловать в личный кабинет методиста!
          </Typography>
          <Typography component="p">
            Здесь вы можете начать работу
          </Typography>
        </Paper>
        <AppBar position="static" className={classes.tabs}>
          <Tabs
            value={ selectedTab }
            onChange={(e, value) => this.tabChange(value)}
            indicatorColor="secondary"
            variant="fullWidth">
              { this.renderTabs() }
          </Tabs>
        </AppBar>
        { this.renderRedirects() }
      </DefaultWrapper>
    )
  }
}

export default withRouter(withStyles(styles)(Methodist))