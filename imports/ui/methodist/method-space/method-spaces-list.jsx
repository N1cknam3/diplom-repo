import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import CardItem from '/imports/ui/components/cardItem';
import Grid from "@material-ui/core/Grid";
import { joinUrl } from '/imports/ui/helpers/functions';
import Typography from '@material-ui/core/Typography';

const styles = {
  title: {
    marginBottom: 10
  },
};

class MethodSpacesList extends Component {

  state = {
    
  }


  renderMethodSpacesList = ({ shortId, materials }) => {
    const { baseUrl } = this.props;
    const categoryUrl = joinUrl(baseUrl, shortId);

    return materials.map(({ title, shortId }) => {
      const url = joinUrl(categoryUrl, shortId)
      return (
        <Grid key={ shortId } item md={3}>
          <CardItem
            title={ title }
            link={ url }/>
        </Grid>
      )
    })
  }


  renderEmptyCategory = () => {
    return (
      <Typography variant="h6" component="h3">
        К сожалению, тут еще нет никаких материалов. Вернитесь позднее..
      </Typography>
    )
  }


  renderCategory = (category) => {
    return (
      <Grid container spacing={24}>
        { this.renderMethodSpacesList(category) }
      </Grid>
    )
  }


  render() {
    const { category } = this.props;
    if (category && category.materials && category.materials.length)
      return this.renderCategory(category);
    return this.renderEmptyCategory();
  }
}

export default withStyles(styles)(MethodSpacesList);