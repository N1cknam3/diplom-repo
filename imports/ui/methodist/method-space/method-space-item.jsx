import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import FileDownload from '/imports/ui/components/FileDownload';
import { getMultilineHtmlText } from '/imports/ui/helpers/functions';
import Typography from '@material-ui/core/Typography';

const styles = {

};

class MethodSpaceItem extends Component {

  state = {
    
  }


  renderParagraph = (key, text) => 
    <Typography key={ key } variant="body1" component="p" paragraph={true}>
      { text }
    </Typography>


  renderDescriptionText = text => {
    if (!text) return (
      <Typography variant="body1" component="i" paragraph={true}>
        Нет описания
      </Typography>
    )
    return getMultilineHtmlText(text, this.renderParagraph)
  }


  renderDownloadButton = () => {
    const { 
      item: {
        fileUrl = '',
        fileName = '',
      }
    } = this.props;
    
    if (!fileUrl) return;
    return <FileDownload fileUrl={fileUrl} fileName={fileName}/>
  }


  render() {
    const { 
      item: {
        title = '',
        text = '',
      }
    } = this.props;
    
    return (
      <div>
        <Typography variant="h6" component="h3" paragraph={true}>
          { title }
        </Typography>
        { this.renderDescriptionText(text) }
        { this.renderDownloadButton() }
      </div>
    )
  }
}

export default withStyles(styles)(MethodSpaceItem);