import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import MethodSpaceItem from '/imports/ui/methodist/method-space/method-space-item';
import MethodSpacesList from '/imports/ui/methodist/method-space/method-spaces-list';
import MethodSpaceCategoriesList from '/imports/ui/methodist/method-space/method-space-categories-list';
import IconButton from '@material-ui/core/IconButton';
import { joinUrl } from '/imports/ui/helpers/functions';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import { Link } from "react-router-dom";
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import { MethodSpaceCategories } from '/imports/api/method-space-categories';

const styles = {
  titleContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20,
    minHeight: 48,
  },
  backButton: {
    marginRight: 10
  }
};

class MethodSpacesSection extends Component {

  state = {
    methodSpaceCategories: [],
  }


  componentWillMount() {
    this.tracker = Tracker.autorun(() => {
      const methodSpaceCategories = MethodSpaceCategories.find({}).fetch();
      this.setState({methodSpaceCategories: methodSpaceCategories});
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  getCategory = categoryId => {
    const { methodSpaceCategories } = this.state;
    return methodSpaceCategories.find(i => i.shortId === categoryId);
  }


  getMethodSpace = (categoryId, itemId) => {
    const category = this.getCategory(categoryId);
    return category.materials.find(i => i.shortId === itemId);
  }


  renderBackButton = url => {
    const { classes } = this.props;
    return (
      <Tooltip title="Назад">
        <IconButton
          className={classes.backButton}
          aria-label="Back"
          component={Link}
          to={url}>
            <KeyboardArrowLeftIcon/>
        </IconButton>
      </Tooltip>
    )
  }


  renderProject = () => {
    const { classes, baseUrl } = this.props;
    const { params: { categoryId, itemId } } = this.props.match;
    const backButton = joinUrl(baseUrl, categoryId);
    return (
      <div>
        <div className={classes.titleContainer}>
          { this.renderBackButton(backButton) }
          <Typography variant="h5" component="h3">
            Информация по методическому пособию
          </Typography>
        </div>
        <MethodSpaceItem
          item={this.getMethodSpace(categoryId, itemId)}/>
      </div>
    )
  }


  renderProjects = () => {
    const { classes, baseUrl } = this.props;
    const { params: { categoryId } } = this.props.match;
    return (
      <div>
        <div className={classes.titleContainer}>
          { this.renderBackButton(baseUrl) }
          <Typography variant="h5" component="h3">
            Список материалов
          </Typography>
        </div>
        <MethodSpacesList
          baseUrl={baseUrl}
          category={this.getCategory(categoryId)}/>
      </div>
    )
  }


  renderCategories = () => {
    const { classes, baseUrl } = this.props;
    const { methodSpaceCategories } = this.state;

    return (
      <div>
        <div className={classes.titleContainer}>
          <Typography variant="h5" component="h3">
            Список категорий методических пособий
          </Typography>
        </div>
        <MethodSpaceCategoriesList baseUrl={baseUrl} categories={methodSpaceCategories}/>
      </div>
    )
  }


  render() {
    const { params: { categoryId, itemId } } = this.props.match;

    if (itemId) return this.renderProject();
    if (categoryId) return this.renderProjects();
    return this.renderCategories();
  }
}

export default withStyles(styles)(MethodSpacesSection);