import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import CompetitionItem from '/imports/ui/methodist/competitions/competition-item';
import CompetitionsList from '/imports/ui/methodist/competitions/competitions-list';
import CompetitionCategoriesList from '/imports/ui/methodist/competitions/competition-categories-list';
import IconButton from '@material-ui/core/IconButton';
import { joinUrl } from '/imports/ui/helpers/functions';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import { Link } from "react-router-dom";
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import { CompetitionCategories } from '/imports/api/competition-categories';

const styles = {
  titleContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20,
    minHeight: 48,
  },
  backButton: {
    marginRight: 10
  }
};

class CompetitionsSection extends Component {

  state = {
    competitionCategories: [],
  }


  componentWillMount() {
    this.tracker = Tracker.autorun(() => {
      const competitionCategories = CompetitionCategories.find({}).fetch();
      this.setState({competitionCategories: competitionCategories});
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  getCategory = categoryId => {
    const { competitionCategories } = this.state;
    return competitionCategories.find(i => i.shortId === categoryId);
  }


  getCompetitionIndex = (categoryId, itemId) => {
    const category = this.getCategory(categoryId);
    return category.competitions.findIndex(i => i.shortId === itemId);
  }


  getCompetition = (categoryId, itemId) => {
    const category = this.getCategory(categoryId);
    const competitionIndex = this.getCompetitionIndex(categoryId, itemId);
    return category.competitions[competitionIndex];
  }


  renderBackButton = url => {
    const { classes } = this.props;
    return (
      <Tooltip title="Назад">
        <IconButton
          className={classes.backButton}
          aria-label="Back"
          component={Link}
          to={url}>
            <KeyboardArrowLeftIcon/>
        </IconButton>
      </Tooltip>
    )
  }


  renderCompetition = () => {
    const { classes, baseUrl } = this.props;
    const { params: { categoryId, itemId } } = this.props.match;
    const backButton = joinUrl(baseUrl, categoryId);
    const category = this.getCategory(categoryId);
    return (
      <div>
        <div className={classes.titleContainer}>
          { this.renderBackButton(backButton) }
          <Typography variant="h5" component="h3">
            Информация по конкурсу
          </Typography>
        </div>
        <CompetitionItem
          item={this.getCompetition(categoryId, itemId)}
          categoryId={category._id}
          competitionIndex={this.getCompetitionIndex(categoryId, itemId)}/>
      </div>
    )
  }


  renderCompetitions = () => {
    const { classes, baseUrl } = this.props;
    const { params: { categoryId } } = this.props.match;
    return (
      <div>
        <div className={classes.titleContainer}>
          { this.renderBackButton(baseUrl) }
          <Typography variant="h5" component="h3">
            Список конкурсов
          </Typography>
        </div>
        <CompetitionsList
          baseUrl={baseUrl}
          category={this.getCategory(categoryId)}/>
      </div>
    )
  }


  renderCategories = () => {
    const { classes, baseUrl } = this.props;
    const { competitionCategories } = this.state;

    return (
      <div>
        <div className={classes.titleContainer}>
          <Typography variant="h5" component="h3">
            Список конкурсных категорий
          </Typography>
        </div>
        <CompetitionCategoriesList baseUrl={baseUrl} categories={competitionCategories}/>
      </div>
    )
  }


  render() {
    const { params: { categoryId, itemId } } = this.props.match;

    if (itemId) return this.renderCompetition();
    if (categoryId) return this.renderCompetitions();
    return this.renderCategories();
  }
}

export default withStyles(styles)(CompetitionsSection);