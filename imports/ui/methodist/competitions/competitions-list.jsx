import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import CardItem from '/imports/ui/components/cardItem';
import Grid from "@material-ui/core/Grid";
import { joinUrl } from '/imports/ui/helpers/functions';
import Typography from '@material-ui/core/Typography';

const styles = {
  title: {
    marginBottom: 10
  },
};

class CompetitionsList extends Component {

  state = {
    
  }


  renderCompetitionsList = ({ shortId, competitions }) => {
    const { baseUrl } = this.props;
    const categoryUrl = joinUrl(baseUrl, shortId);

    return competitions.map(({ title, shortId }) => {
      const url = joinUrl(categoryUrl, shortId)
      return (
        <Grid key={ shortId } item md={3}>
          <CardItem
            title={ title }
            link={ url }/>
        </Grid>
      )
    })
  }


  renderEmptyCategory = () => {
    return (
      <Typography variant="h6" component="h3">
        К сожалению, тут еще нет активных конкурсов. Вернитесь позднее..
      </Typography>
    )
  }


  renderCategory = (category) => {
    return (
      <Grid container spacing={24}>
        { this.renderCompetitionsList(category) }
      </Grid>
    )
  }


  render() {
    const { category } = this.props;
    if (category && category.competitions && category.competitions.length)
      return this.renderCategory(category);
    return this.renderEmptyCategory();
  }
}

export default withStyles(styles)(CompetitionsList);