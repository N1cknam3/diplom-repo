import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import ProjectItem from '/imports/ui/methodist/projects/project-item';
import ProjectsList from '/imports/ui/methodist/projects/projects-list';
import ProjectCategoriesList from '/imports/ui/methodist/projects/project-categories-list';
import IconButton from '@material-ui/core/IconButton';
import { joinUrl } from '/imports/ui/helpers/functions';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import { Link } from "react-router-dom";
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import { ProjectCategories } from '/imports/api/project-categories';

const styles = {
  titleContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20,
    minHeight: 48,
  },
  backButton: {
    marginRight: 10
  }
};

class ProjectsSection extends Component {

  state = {
    projectCategories: [],
  }


  componentWillMount() {
    this.tracker = Tracker.autorun(() => {
      const projectCategories = ProjectCategories.find({}).fetch();
      this.setState({projectCategories: projectCategories});
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  getCategory = categoryId => {
    const { projectCategories } = this.state;
    return projectCategories.find(i => i.shortId === categoryId);
  }


  getProjectIndex = (categoryId, itemId) => {
    const category = this.getCategory(categoryId);
    return category.projects.findIndex(i => i.shortId === itemId);
  }


  getProject = (categoryId, itemId) => {
    const category = this.getCategory(categoryId);
    const projectIndex = this.getProjectIndex(categoryId, itemId);
    return category.projects[projectIndex];
  }


  renderBackButton = url => {
    const { classes } = this.props;
    return (
      <Tooltip title="Назад">
        <IconButton
          className={classes.backButton}
          aria-label="Back"
          component={Link}
          to={url}>
            <KeyboardArrowLeftIcon/>
        </IconButton>
      </Tooltip>
    )
  }


  renderProject = () => {
    const { classes, baseUrl } = this.props;
    const { params: { categoryId, itemId } } = this.props.match;
    const backButton = joinUrl(baseUrl, categoryId);
    const category = this.getCategory(categoryId);
    return (
      <div>
        <div className={classes.titleContainer}>
          { this.renderBackButton(backButton) }
          <Typography variant="h5" component="h3">
            Информация по конкурсу проектов
          </Typography>
        </div>
        <ProjectItem
          item={this.getProject(categoryId, itemId)}
          categoryId={category._id}
          projectIndex={this.getProjectIndex(categoryId, itemId)}/>
      </div>
    )
  }


  renderProjects = () => {
    const { classes, baseUrl } = this.props;
    const { params: { categoryId } } = this.props.match;
    return (
      <div>
        <div className={classes.titleContainer}>
          { this.renderBackButton(baseUrl) }
          <Typography variant="h5" component="h3">
            Список проектных конкурсов
          </Typography>
        </div>
        <ProjectsList
          baseUrl={baseUrl}
          category={this.getCategory(categoryId)}/>
      </div>
    )
  }


  renderCategories = () => {
    const { classes, baseUrl } = this.props;
    const { projectCategories } = this.state;

    return (
      <div>
        <div className={classes.titleContainer}>
          <Typography variant="h5" component="h3">
            Список категорий конкурсных проектов
          </Typography>
        </div>
        <ProjectCategoriesList baseUrl={baseUrl} categories={projectCategories}/>
      </div>
    )
  }


  render() {
    const { params: { categoryId, itemId } } = this.props.match;

    if (itemId) return this.renderProject();
    if (categoryId) return this.renderProjects();
    return this.renderCategories();
  }
}

export default withStyles(styles)(ProjectsSection);