import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import FileUpload from '/imports/ui/components/FileUpload';
import LinearProgress from '@material-ui/core/LinearProgress';
import { storageProjectApplicationsRef } from '/imports/api/firebase';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import { ProjectCategories } from '/imports/api/project-categories';


const styles = theme => ({
  
});


class RegisterProject extends Component {

  state = {
    documentTitle: 'Регистрация участия в конкурсе проектов',
    application: {
      title: '',
      userId: Meteor.userId(),
      createdAt: new Date(),
      updatedAt: new Date(),
      fileName: '',
      fileUrl: '',
    },
    selectedFile: null,
    savingProcessing: false,
    uploadProgress: 0,
  }


  componentWillMount() {
    const { application } = this.props;
    if (application) {
      this.setState({ documentTitle: 'Изменение заявки на участие' });
      this.setState({ application: application });
    }
  }


  setStateApplication = (fieldName, value) => {
    this.setState({
      application: {
        ...this.state.application,
        [fieldName]: value
      }
    })
  }


  handleInputChange = name => event => {
    const { value } = event.target;
    this.setStateApplication(name, value);
  }


  selectFileHandler = file => {
    this.setState({ selectedFile: file });
    this.setStateApplication('fileUrl', URL.createObjectURL(file));
  }


  removeFileHandler = () => {
    this.setStateApplication('fileUrl', '');
    this.setState({ selectedFile: null });
  }


  renderParagraph = (key, text) => 
    <Typography key={ key } variant="body1" component="p" paragraph={true}>
      { text }
    </Typography>


  generateFileName = oldName => 
    new Date().getTime().toString() + '.' + oldName.split('.').pop();


  setProgressOpacity = () => {
    const { uploadProgress } = this.state;
    return uploadProgress === 0 || uploadProgress === 100 ? 0 : 1;
  }


  addApplicationToSource = application => {
    const { categoryId, projectIndex } = this.props;
    application.createdAt = new Date();
    ProjectCategories.update(categoryId, {
      $push: {
        ['projects.' + projectIndex + '.applications']: application
      }
    });
  }


  updateApplicationToSource = application => {
    const { categoryId, projectIndex, applicationIndex } = this.props;
    application.updatedAt = new Date();
    const result = ProjectCategories.update(categoryId, {
      $set: {
        ['projects.' + projectIndex + '.applications.' + applicationIndex]: application
      }
    });
    return result;
  }


  processUploadToServer = (file, afterUploadHandler = () => {}) => {
    const { application } = this.props;
    /** Удаление предыдущего файла, если он есть (по изначальному состоянию) */
    if (application && application.fileName) this.processDeletingFromServer(application.fileName);
    const newFileName = this.generateFileName(file.name);
    this.uploadFileToServer(newFileName, file).then(url => {
      this.setStateApplication('fileUrl', url);
      this.setStateApplication('fileName', newFileName);
      afterUploadHandler();
    })
  }


  processDeletingFromServer = (fileName, afterDeleteHandler = () => {}) => {
    this.deleteFileFromServer(fileName).then(
      () => {
        this.setStateApplication('fileName', '');
        afterDeleteHandler();
      },
      (res) => {
        if (res.code === "storage/object-not-found") {
          /** Файл уже был удален с сервера => удалить упоминание о нем из БД */
          this.setStateApplication('fileName', '');
          afterDeleteHandler();
        }
      }
    );
  }


  uploadFileToServer = (fileName, file) => {
    const uploadTask = storageProjectApplicationsRef.child(fileName).put(file);
    uploadTask.on('state_changed', 
      (snapshot) => {
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({ uploadProgress: progress });
      },
      (error) => {
        console.error(error);
      });
    return uploadTask
      .then(() => storageProjectApplicationsRef.child(fileName).getDownloadURL());
  }


  deleteFileFromServer = fileName => {
    return storageProjectApplicationsRef.child(fileName).delete();
  }


  handleSubmit = e => {
    const { application, selectedFile } = this.state;
    e.preventDefault();
    if (!application) return;
    this.setState({ savingProcessing: true });
    if (selectedFile)
      /** Файл выбран, значит его надо загрузить */
      this.processUploadToServer(selectedFile, this.submitApplication);
    else this.submitApplication();
  }


  submitApplication = () => {
    //  TODO: сделать серверный метод на сохранение, где проверять еще и shortId, что такой уже занят
    const { handleClose, applicationIndex } = this.props;
    const { application } = this.state;
    if (applicationIndex === -1) this.addApplicationToSource(application);
    else this.updateApplicationToSource(application);
    this.setState({ savingProcessing: false });
    handleClose();
  }


  render() {
    const { handleClose } = this.props;
    const { application, documentTitle, selectedFile, uploadProgress } = this.state;
    const {
      title = '',
    } = application;
    const fileName = selectedFile ? selectedFile.name
      : application.fileUrl && application.fileName ? application.fileName
      : '';

    return (
      <form 
        noValidate 
        autoComplete="off" 
        onSubmit={e => this.handleSubmit(e)}>
          <DialogContent>
            <DialogContentText>
              { documentTitle }
            </DialogContentText>
              <TextField
                autoFocus
                id="title"
                margin="dense"
                label="Название организации"
                type="text"
                multiline
                rowsMax="4"
                value={title}
                onChange={this.handleInputChange('title')}
                fullWidth/>
              <Typography variant="body1" component="p" paragraph={true}>
                Для регистрации участия в конкурсе проектов необходимо загрузить список участников
              </Typography>
              <FileUpload
                  onSelected={ file => this.selectFileHandler(file) }
                  onRemove={ () => this.removeFileHandler() }
                  fileName={ fileName }/>
          </DialogContent>
          <LinearProgress
            style={{
              opacity: this.setProgressOpacity()
            }}
            variant="determinate"
            value={uploadProgress}/>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Отмена
            </Button>
            <Button color="primary" type="submit">
              Готово
            </Button>
          </DialogActions>
      </form>
    )
  }
}

export default withStyles(styles, { withTheme: true })(RegisterProject);