import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import CardItem from '/imports/ui/components/cardItem';
import Grid from "@material-ui/core/Grid";
import { joinUrl } from '/imports/ui/helpers/functions';
import Typography from '@material-ui/core/Typography';

const styles = {
  title: {
    marginBottom: 10
  },
};

class ProjectsList extends Component {

  state = {
    
  }


  renderProjectsList = ({ shortId, projects }) => {
    const { baseUrl } = this.props;
    const categoryUrl = joinUrl(baseUrl, shortId);

    return projects.map(({ title, shortId }) => {
      const url = joinUrl(categoryUrl, shortId)
      return (
        <Grid key={ shortId } item md={3}>
          <CardItem
            title={ title }
            link={ url }/>
        </Grid>
      )
    })
  }


  renderEmptyCategory = () => {
    return (
      <Typography variant="h6" component="h3">
        К сожалению, тут еще нет активных проектных конкурсов. Вернитесь позднее..
      </Typography>
    )
  }


  renderCategory = (category) => {
    return (
      <Grid container spacing={24}>
        { this.renderProjectsList(category) }
      </Grid>
    )
  }


  render() {
    const { category } = this.props;
    if (category && category.projects && category.projects.length)
      return this.renderCategory(category);
    return this.renderEmptyCategory();
  }
}

export default withStyles(styles)(ProjectsList);