import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import DialogWindow from '/imports/ui/components/DialogWindow';
import Fab from '@material-ui/core/Fab';
import { getMultilineHtmlText, isActiveDate } from '/imports/ui/helpers/functions';
import RegisterProject from '/imports/ui/methodist/projects/register-project';
import BeenhereIcon from '@material-ui/icons/Beenhere';
import Typography from '@material-ui/core/Typography';
import { Meteor } from 'meteor/meteor';
import moment from 'moment';

import { teal, orange } from '@material-ui/core/colors';

const styles = {
  contentContainer: {
    marginBottom: 30,
  },
  registerIcon: {
    marginRight: 10,
  },
  registerButton: {
    marginBottom: 20,
    backgroundColor: teal[400],
    color: teal[50],
    '&:hover': {
      backgroundColor: teal[500],
    }
  },
  editRegisterButton: {
    marginBottom: 20,
    backgroundColor: orange[400],
    color: orange[50],
    '&:hover': {
      backgroundColor: orange[500],
    }
  },
};

class ProjectItem extends Component {

  state = {
    dialogOpened: false,
  }


  handleRegister = () => {
    this.setState({ dialogOpened: true });
  }


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
  }


  getUserApplicationIndex = () => {
    const { item } = this.props;
    if (!item.applications || !item.applications.length) return -1;
    const userId = Meteor.userId();
    return item.applications.findIndex(i => i.userId === userId);
  }


  getUserApplication = index => {
    const { item } = this.props;
    const i = index ? index : this.getUserApplicationIndex();
    if (i === -1) return;
    return item.applications[i];
  }


  getApplicationsCount = () => {
    const { item } = this.props;
    if (!item.applications || !item.applications.length) return 0;
    return item.applications.length;
  }


  renderParagraph = (key, text) => 
    <Typography key={ key } variant="body1" component="p" paragraph={true}>
      { text }
    </Typography>


  renderDialogContent = () => {
    const { categoryId, projectIndex } = this.props;
    const applicationIndex = this.getUserApplicationIndex();
    const application = this.getUserApplication(applicationIndex);

    return (
      <RegisterProject
        categoryId={categoryId}
        projectIndex={projectIndex}
        application={application}
        applicationIndex={applicationIndex}
        handleClose={this.handleDialogClose}/>
    )
  }


  renderRegistrationButton = () => {
    const { classes } = this.props;

    return (
      <Fab
        className={classes.registerButton}
        variant="extended"
        aria-label="Add"
        onClick={ () => this.handleRegister() }>
          <BeenhereIcon className={classes.registerIcon}/>
          Подать заявку
      </Fab>
    )
  }


  renderEditRegistrationButton = () => {
    const { classes } = this.props;

    return (
      <Fab
        className={classes.editRegisterButton}
        variant="extended"
        aria-label="Add"
        onClick={ () => this.handleRegister() }>
          <BeenhereIcon className={classes.registerIcon}/>
          Изменить заявку
      </Fab>
    )
  }


  renderButton = () => {
    const application = this.getUserApplication();
    if (!application) return this.renderRegistrationButton();
    return this.renderEditRegistrationButton();
  }


  renderDescriptionText = text => {
    if (!text) return (
      <Typography variant="body1" component="i" paragraph={true}>
        Нет описания
      </Typography>
    )
    return (
      <div>
        <hr/>
        { getMultilineHtmlText(text, this.renderParagraph) }
        <hr/>
      </div>
    )
  }


  renderApplicationSection = () => {
    const { dialogOpened } = this.state;
    return (
      <div>
        <Typography variant="body1" component="p">
          <i>Идет прием заявок на участие.</i>
        </Typography>
        { this.renderApplicationsCount() }
        { this.renderButton() }
        <DialogWindow
          open={dialogOpened}
          handleClose={this.handleDialogClose}>
            { this.renderDialogContent() }
        </DialogWindow>
      </div>
    )
  }


  renderEndedSection = () => {
    return (
      <div>
        <Typography variant="body1" component="p">
          <i>Прием заявок завершен.</i>
        </Typography>
        { this.renderApplicationsCount() }
      </div>
    )
  }


  renderApplicationsCount = () => {
    const count = this.getApplicationsCount();
    return (
      <Typography variant="overline" component="h3" paragraph={true}>
        <b>Кол-во участников:</b> { count }
      </Typography>
    )
  }


  render() {
    const { dialogOpened } = this.state; 
    const { 
      classes,
      item: {
        title = '',
        endDateAt = new Date(),
        text = '',
      }
    } = this.props;

    const isActive = isActiveDate(endDateAt);
    const dateEndingText = moment(endDateAt).endOf('day').format('LLLL');
    
    return (
      <div>
        <div className={classes.contentContainer}>
          <Typography variant="h6" component="h3" paragraph={true}>
            { title }
          </Typography>
          <Typography variant="overline" component="h3" paragraph={true}>
            <b>Дата окончания:</b> { dateEndingText }
          </Typography>
          { this.renderDescriptionText(text) }
        </div>
        { isActive ? this.renderApplicationSection() : this.renderEndedSection() }
      </div>
    )
  }
}

export default withStyles(styles)(ProjectItem);