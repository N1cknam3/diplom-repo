import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import CardItem from '/imports/ui/components/cardItem';
import Grid from "@material-ui/core/Grid";
import { joinUrl } from '/imports/ui/helpers/functions';

const styles = {
  title: {
    marginBottom: 10
  },
};

class ProjectCategoriesList extends Component {

  state = {
    
  };


  renderCategoriesList = () => {
    const { baseUrl, categories } = this.props;

    return categories.map(({ name, shortId }) => {
      const url = joinUrl(baseUrl, shortId);
      return (
        <Grid key={ shortId } item md={3}>
          <CardItem
            title={ name }
            link={ url }/>
        </Grid>
      )
    })
  }


  render() {
    return (
      <Grid container spacing={24}>
        { this.renderCategoriesList() }
      </Grid>
    )
  }
}

export default withStyles(styles)(ProjectCategoriesList);