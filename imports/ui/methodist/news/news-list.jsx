import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import { _ } from 'underscore';
import CardItem from '/imports/ui/components/cardItem';
import Grid from "@material-ui/core/Grid";
import moment from 'moment';

const styles = {

};

class NewsList extends Component {

  state = {
    
  }


  renderNews = () => {
    const { baseUrl, news } = this.props;

    return _.map(news, ({ title, shortId, imageUrl, createdAt }) => {
      const link = baseUrl + "/" + shortId;
      const date = moment(createdAt).format('DD-MM-YYYY');
      return (
        <Grid key={ title } item md={3}>
          <CardItem
            title={ title }
            description={ date }
            renderImage={ true }
            image={ imageUrl }
            link={ link }/>
        </Grid>
      )
    })
  }


  render() {
    return (
      <Grid container spacing={24}>
        { this.renderNews() }
      </Grid>
    )
  }
}

export default withStyles(styles)(NewsList);