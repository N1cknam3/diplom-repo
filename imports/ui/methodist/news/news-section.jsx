import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import { _ } from 'underscore';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import { Link } from "react-router-dom";
import NewsItem from '/imports/ui/methodist/news/news-item';
import NewsList from '/imports/ui/methodist/news/news-list';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import { News } from '/imports/api/news';

const styles = {
  titleContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20,
    minHeight: 48,
  },
  backButton: {
    marginRight: 10
  },
};

class NewsSection extends Component {

  state = {
    news: []
  }


  componentWillMount() {
    this.tracker = Tracker.autorun(() => {
      const news = News.find({}, { sort: { createdAt: -1 } }).fetch();
      this.setState({ news: news });
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }
  
  
  getNews = newsId => {
    const { news } = this.state;
    return news.find(i => i.shortId === newsId);
  }


  renderBackButton = url => {
    const { classes } = this.props;
    return (
      <Tooltip title="Назад">
        <IconButton
          className={classes.backButton}
          aria-label="Back"
          component={Link}
          to={url}>
            <KeyboardArrowLeftIcon/>
        </IconButton>
      </Tooltip>
    )
  }


  renderNewsItem = () => {
    const { classes, baseUrl, match: { params: { itemId } } } = this.props;
    const newsItem = this.getNews(itemId);

    return (
      <div>
        <div className={classes.titleContainer}>
          { this.renderBackButton(baseUrl) }
          <Typography variant="h5" component="h3" className={classes.title}>
            Подробная информация
          </Typography>
        </div>
        <NewsItem baseUrl={baseUrl} item={newsItem}/>
      </div>
    )
  }


  renderNewsList = () => {
    const { classes, baseUrl } = this.props;
    const { news } = this.state;

    return (
      <div>
        <div className={classes.titleContainer}>
          <Typography variant="h5" component="h3" className={classes.title}>
            Новости
          </Typography>
        </div>
        <NewsList baseUrl={baseUrl} news={news}/>
      </div>
    )
  }


  render() {
    const { match: { params: { itemId } } } = this.props;

    if (itemId) return this.renderNewsItem();
    return this.renderNewsList();
  }
}

export default withStyles(styles)(NewsSection);