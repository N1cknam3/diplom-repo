import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

import { getMultilineHtmlText } from '/imports/ui/helpers/functions';
import Typography from '@material-ui/core/Typography';

const styles = {
  imageContainer: {
    overflow: 'hidden',
    width: '100%',
    height: 200,
    display: 'flex',
    alignItems: 'center',
    marginBottom: 16,
    backgroundColor: '#eee',
  },
  image: {
    width: '100%',
  }
};

class NewsItem extends Component {

  state = {
    
  }


  renderImage = (src, alt) => {
    const { classes } = this.props;
    if (!src) return;
    return (
      <div className={classes.imageContainer}>
        <img className={classes.image} src={src} alt={alt}/>
      </div>
    )
  }


  renderParagraph = (key, text) => 
    <Typography key={ key } variant="body1" component="p" paragraph={true}>
      { text }
    </Typography>


  renderDescriptionText = text => {
    if (!text) return (
      <Typography variant="body1" component="i" paragraph={true}>
        Нет описания
      </Typography>
    )
    return getMultilineHtmlText(text, this.renderParagraph)
  }


  render() {
    const {
      item: {
        title = '',
        text = '',
        imageUrl = '',
      }
    } = this.props;
    
    return (
      <div>
        <Typography variant="h6" component="h3" paragraph={true}>
          { title }
        </Typography>
        { this.renderImage(imageUrl, title) }
        { this.renderDescriptionText(text) }
      </div>
    )
  }
}

export default withStyles(styles)(NewsItem);