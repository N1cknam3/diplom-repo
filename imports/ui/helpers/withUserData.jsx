import { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

class WithUserData extends Component {

  state = {
    user: null
  };

  componentWillMount() {
    this.setUserData();
  }

  setUserData = () => {
    Tracker.autorun(() => {
      const userDataHandle = Meteor.subscribe('getUserWithRoles');
      if (userDataHandle.ready() && !this.state.user) Tracker.nonreactive(() => {
        this.setState({ user: Meteor.user() });
      });
    });
  };

  render() {
    if (!this.state.user) return null;
    return this.props.render();
  }
}


export default WithUserData;
