import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import WithUserData from '/imports/ui/helpers/withUserData';

export default class PrivateRoutes extends Component {
  checkConditionsAndRenderContentIfAllowed = (props) => {
    const user = Meteor.user();
    if (!user.roles) return <Redirect to="/logout"/>;
    if (props.toRender && user.roles.includes(props.role)) return props.toRender();
    if (props.toRenderAdmin && user.roles.includes('admin')) return props.toRenderAdmin();
    if (props.toRenderMethodist && user.roles.includes('methodist')) return props.toRenderMethodist();
    return <Redirect to="/login"/>;
  };

  render() {
    if (!Meteor.userId()) return <Redirect to="/login"/>;
    return <WithUserData render={() => this.checkConditionsAndRenderContentIfAllowed(this.props)}/>;
  }
}

PrivateRoutes.propTypes = {
  toRender: PropTypes.func,
  toRenderAdmin: PropTypes.func,
  toRenderMethodist: PropTypes.func
};
