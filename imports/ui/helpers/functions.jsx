import moment from 'moment';

joinUrl = (base, path) => 
  base.charAt(base.length - 1) === '/'
    ? base + path
    : base + '/' + path


getMultilineHtmlText = (text, wrappingHandler) => 
  text.split("\n").map((textLine, key) => {
    return wrappingHandler(key, textLine)
  })


isActiveDate = (dateEnding) => {
  const current = moment();
  const dateEnding_endOfDay = moment(dateEnding).endOf('day');
  return moment(current).isBefore(dateEnding_endOfDay);
}


export {
  joinUrl,
  getMultilineHtmlText,
  isActiveDate,
}