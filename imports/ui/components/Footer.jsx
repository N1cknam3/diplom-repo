import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import grey from '@material-ui/core/colors/grey';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    position: 'relative',
    bottom: 0,
    width: '100%',
    height: 100,
    backgroundColor: grey[200],
  },
  toolbar: {
    marginLeft: 'auto',
    marginRight: 'auto',
    [theme.breakpoints.down('sm')]: { //  600px
      width: '100%'
    },
    [theme.breakpoints.up('sm')]: { //  600px
      width: 500
    },
    [theme.breakpoints.up('md')]: { //  960px
      width: 768
    },
    [theme.breakpoints.up('lg')]: { //  1280px
      width: 1024
    },
  },
  text: {
    color: grey[500]
  }
});

class Footer extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Toolbar className={classes.toolbar}>
          <Typography component="p" className={classes.text}>&copy; 2018 &mdash; 2019 Плетнева Ирина</Typography>
        </Toolbar>
      </div>
    );
  }
}

export default withStyles(styles)(Footer)