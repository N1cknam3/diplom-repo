import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import LinesEllipsis from 'react-lines-ellipsis'
import { Link } from "react-router-dom";
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  cardAction: {
    flexGrow: 1,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  text: {
    color: 'rgba(0, 0, 0, 0.87)',
    fontSize: '0.875rem',
    fontWeight: 400,
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    lineHeight: '1.46429em',
  },
  media: {
    height: 140,
    backgroundColor: '#eee',
  },
  mediaPlaceholder: {
    height: 140,
    display: 'block',
    backgroundColor: '#eee',
  },
};


class CardItem extends Component {

  renderImage = () => {
    const { classes, title, image } = this.props;
    if (!image) return;
    return (
      <CardMedia
        className={ classes.media }
        image={ image }
        title={ title }
      />  
    )
  }


  renderImagePlaceholder = () => {
    const { classes } = this.props;
    return <div className={ classes.mediaPlaceholder }/>  
  }


  render() {
    const { classes, title, description, link, renderImage } = this.props;

    return (
      <Card className={classes.card}>
        <CardActionArea
          className={classes.cardAction}
          component={ Link }
          to={ link }>
            { renderImage && (this.renderImage() || this.renderImagePlaceholder()) }
            <CardContent>
              <Typography className={classes.title} color="textSecondary" gutterBottom>
                { description }
              </Typography>
              <LinesEllipsis
                text={title}
                component="p"
                maxLine='2'
                className={classes.text}
                ellipsis='...'
                trimRight
                basedOn='letters'
              />
            </CardContent>
        </CardActionArea>
      </Card>
    )
  }
}

export default withStyles(styles)(CardItem);