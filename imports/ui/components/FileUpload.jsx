import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles'

import AttachmentIcon from '@material-ui/icons/Attachment';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import DeleteIcon from '@material-ui/icons/Delete';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

const styles = theme => ({
  mainContainer: {
    width: 'fit-content',
    marginTop: 10,
  },
  card: {
    width: 'fit-content',
    height: '100%',
    marginTop: 10,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  cardAction: {
    flexGrow: 1,
    width: 200,
    height: 140,
    display: 'flex',
    '&:hover $focusHighlight': {
      opacity: 0.3,
    },
  },
  cardActionDelete: {
    '&:hover $cardContentDelete': {
      color: '#fff',
      opacity: 1
    }
  },
  cardActionUpload: {
    '&:hover $cardContentUpload': {
      color: '#000'
    }
  },
  focusHighlight: {},
  cardContent: {
    transition: '.2s',
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    fontSize: 16,
    zIndex: 1,
    top: '50%',
    left: '50%',
    transform: 'translateX(-50%) translateY(-50%)',
  },
  cardContentDelete: {
    opacity: 0,
    color: '#fff',  
  },
  cardContentUpload: {
    color: '#666',
  },
  media: {
    width: 200,
    height: 140,
    backgroundColor: '#eee',
  },
  mediaPlaceholder: {
    width: 200,
    height: 140,
    display: 'block',
    backgroundColor: '#eee',
  },
  backgroundIcon: {
    fontSize: 72,
    color: 'rgba(0,0,0,0.2)',
  },
  fileNameText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    color: 'rgba(0,0,0,0.5)',
    paddingRight: 10,
  },
  icon: {
    marginRight: 5,
  }
});

class FileUpload extends Component {

  state = {
    
  }


  selectButtonHandler = ({ target = {} }) => {
    const { onSelected } = this.props;
    const file = target.files[0];
    if (!file) return;
    onSelected(file);
  }


  deleteButtonHandler() {
    const { onRemove } = this.props;
    onRemove();
  }


  renderImageDeleteButton = () => {
    const { classes, fileName = 'document.document' } = this.props;

    return (
      <Card className={ classes.card }>
        <CardActionArea
          title={ "Удалить файл" }
          className={[ classes.cardAction, classes.cardActionDelete ].join(' ')}
          classes={{ focusHighlight: classes.focusHighlight }}
          onClick={() => this.deleteButtonHandler()}>
            <InsertDriveFileIcon
              className={classes.backgroundIcon}/>
            <span className={classes.fileNameText}>
              { fileName }
            </span>
            <CardContent 
              className={[ classes.cardContent, classes.cardContentDelete ].join(' ')}>  
                <DeleteIcon className={classes.icon}/>
                Удалить
            </CardContent>
        </CardActionArea>
      </Card>
    )
  }


  renderImageUploadButton = () => {
    const { classes } = this.props;

    return (
      <Card className={ classes.card }>
        <input
          accept=".pdf, .doc, .docx"
          className={ classes.input }
          style={{ display: 'none' }}
          type="file"
          ref={ ref => this.uploadInput = ref }
          onChange={ this.selectButtonHandler }
        />
        <CardActionArea
          title={ "Загрузить файл" }
          className={[ classes.cardAction, classes.cardActionUpload ].join(' ')}
          onClick={() => { this.uploadInput.click() }}>
            <div className={ classes.mediaPlaceholder }/>
            <CardContent
              className={[ classes.cardContent, classes.cardContentUpload ].join(' ')}>  
                <AttachmentIcon className={classes.icon}/>
                Загрузить
            </CardContent>
        </CardActionArea>
      </Card>
    )
  }


  render() {
    const { fileName } = this.props;
    return fileName 
      ? this.renderImageDeleteButton()
      : this.renderImageUploadButton();
  }
}

export default withStyles(styles, { withTheme: true })(FileUpload)