import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginBottom: 75
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  toolbar: {
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: 0,
    [theme.breakpoints.down('sm')]: { //  600px
      width: '100%'
    },
    [theme.breakpoints.up('sm')]: { //  600px
      width: 500
    },
    [theme.breakpoints.up('md')]: { //  960px
      width: 768
    },
    [theme.breakpoints.up('lg')]: { //  1280px
      width: 1024
    },
  }
});

class Header extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="fixed">
          <Toolbar className={classes.toolbar}>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              {this.props.title}
            </Typography>
            <Button component={Link} to="/logout" color="inherit">Выйти</Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles)(Header)