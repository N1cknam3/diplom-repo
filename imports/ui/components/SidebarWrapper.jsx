import React, { Component } from 'react';
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import ReceiptIcon from '@material-ui/icons/Receipt';
import SpeakerNotesIcon from '@material-ui/icons/SpeakerNotes';
import TabIcon from '@material-ui/icons/Tab';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';


const drawerWidth = 240;


const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  flexGrow: {
    flexGrow: 1
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
});


class SidebarWrapper extends Component {

  state = {
    mobileOpen: false,
  };


  toggleDrawer = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };


  closeDrawer = () => {
    this.setState(state => ({ mobileOpen: false }));
  };


  renderDrawer = () => {
    const { classes } = this.props;

    return (
      <div>
        <div className={classes.toolbar} />
        <Divider />
        <List>
          <ListItem button component={Link} to="/news" onClick={this.closeDrawer}>
            <ListItemIcon><InfoIcon/></ListItemIcon>
            <ListItemText primary="Новости" />
          </ListItem>
          <ListItem button component={Link} to="/method-spaces" onClick={this.closeDrawer}>
            <ListItemIcon><SpeakerNotesIcon/></ListItemIcon>
            <ListItemText primary="Методическое пространство" />
          </ListItem>
          <ListItem button component={Link} to="/competitions" onClick={this.closeDrawer}>
            <ListItemIcon><ReceiptIcon/></ListItemIcon>
            <ListItemText primary="Конкурсы" />
          </ListItem>
          <ListItem button component={Link} to="/projects" onClick={this.closeDrawer}>
            <ListItemIcon><TabIcon/></ListItemIcon>
            <ListItemText primary="Проекты" />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button component={Link} to="/logout" onClick={this.closeDrawer}>
            <ListItemIcon><ExitToAppIcon/></ListItemIcon>
            <ListItemText primary="Выйти" />
          </ListItem>
        </List>
      </div>
    );
  }


  render() {
    const { classes, theme, title } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.toggleDrawer}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap className={classes.flexGrow}>{title}</Typography>
            <Button component={Link} to="/logout" color="inherit">Выйти</Button>
          </Toolbar>
        </AppBar>
        <nav className={classes.drawer}>
          <Hidden smUp implementation="css">
            <Drawer
              container={this.props.container}
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={this.state.mobileOpen}
              onClose={this.toggleDrawer}
              classes={{
                paper: classes.drawerPaper
              }}
            >
              {this.renderDrawer()}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              {this.renderDrawer()}
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {this.props.children}
        </main>
      </div>
    )
  }
}

SidebarWrapper.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(SidebarWrapper))