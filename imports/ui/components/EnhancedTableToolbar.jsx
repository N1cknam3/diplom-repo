import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import TimelineIcon from '@material-ui/icons/Timeline';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import { Link } from "react-router-dom";
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
      color: theme.palette.secondary.main,
      backgroundColor: lighten(theme.palette.secondary.light, 0.85),
    }
      : {
      color: theme.palette.text.primary,
      backgroundColor: theme.palette.secondary.dark,
    },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    display: 'flex',
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});
  
class EnhancedTableToolbar extends Component {

  renderBackButton(backButtonLink) {
    if (!backButtonLink) return;
    return (
      <Tooltip title="Назад">
        <IconButton
          aria-label="Back"
          component={Link}
          to={backButtonLink}>
            <KeyboardArrowLeftIcon/>
        </IconButton>
      </Tooltip>
    )
  }
  
  render() {
    const { numSelected, classes, handleOpen, selectedItems, tableTitle, backButtonLink } = this.props;

    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
          })}
      >
        {this.renderBackButton(backButtonLink)}
        <div className={classes.title}>
          {numSelected > 0 ? (
              <Typography color="inherit" variant="subtitle1">
                {numSelected} выбрано
              </Typography>
          ) : (
              <Typography variant="h6" id="tableTitle">
                { tableTitle ? tableTitle : 'Архив новостей' }
              </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          <Tooltip title="Добавить">
            <IconButton
              aria-label="Add"
              onClick={(e) => handleOpen('add')}
            >
              <AddIcon/>
            </IconButton>
          </Tooltip>
          {numSelected > 0 ? (
            <Tooltip title="Удалить">
              <IconButton
                aria-label="Delete"
                onClick={(e) => handleOpen('delete', selectedItems)} 
              >
                <DeleteIcon/>
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title="Показать график">
              <IconButton 
                aria-label="Filter list"
                onClick={(e) => handleOpen('chart', selectedItems)}
              >
                <TimelineIcon />
              </IconButton>
            </Tooltip>
          )}
        </div>
      </Toolbar>
    )
  }
}

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  handleOpen: PropTypes.func.isRequired,
  selectedItems: PropTypes.array,
  tableTitle: PropTypes.string,
  backButtonLink: PropTypes.string,
};

export default withStyles(toolbarStyles)(EnhancedTableToolbar)