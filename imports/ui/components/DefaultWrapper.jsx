import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import Footer from '/imports/ui/components/Footer';
import Header from '/imports/ui/components/Header';

const styles = theme => ({
  content: {
    display: 'flex',
    minHeight: 'calc(100vh - 75px)',
    flexDirection: 'column'
  },
  main: {
    flex: '1 0 auto',
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 10,
    [theme.breakpoints.down('sm')]: { //  600px
      width: '100%'
    },
    [theme.breakpoints.up('sm')]: { //  600px
      width: 500
    },
    [theme.breakpoints.up('md')]: { //  960px
      width: 768
    },
    [theme.breakpoints.up('lg')]: { //  1280px
      width: 1024
    },
  }
});

class DefaultPageWrapper extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Header title={this.props.title}/>
        <div className={classes.content}>
          <div className={classes.main}>
            {this.props.children}
          </div>
          <Footer/>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(DefaultPageWrapper)