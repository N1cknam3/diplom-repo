import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Moment from 'react-moment';

import Checkbox from '@material-ui/core/Checkbox';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import EnhancedTableHead from '/imports/ui/components/EnhancedTableHead'
import EnhancedTableToolbar from '/imports/ui/components/EnhancedTableToolbar'
import IconButton from '@material-ui/core/IconButton';
import { Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';


function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) return -1;
  if (b[orderBy] > a[orderBy]) return 1;
  return 0;
}


function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}


function getSorting(order, orderBy) {
  return order === 'desc' 
    ? (a, b) => desc(a, b, orderBy) 
    : (a, b) => -desc(a, b, orderBy);
}


const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  }
});


class PaginationTable extends Component {

  state = {
    order: 'desc',
    orderBy: 'createdAt',
    selected: [],
    page: 0,
    rowsPerPage: 10,
  };


  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };


  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n._id) }));
      return;
    }
    this.setState({ selected: [] });
  };


  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) { newSelected = newSelected.concat(selected, id) } 
    else if (selectedIndex === 0) { newSelected = newSelected.concat(selected.slice(1)) }
    else if (selectedIndex === selected.length - 1) { newSelected = newSelected.concat(selected.slice(0, -1)) }
    else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };


  handleChangePage = (event, page) => {
    this.setState({ page });
  };


  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };


  isSelected = id => this.state.selected.indexOf(id) !== -1;


  renderFields(item, fields) {
    return fields.map(field => {
      return (
        <TableCell
          key={field}
          onClick={event => this.handleClick(event, item._id)}>
            {item[field]}
        </TableCell>
      )
    })
  }


  renderActions(item, actions) {
    if (!actions || !actions.length) return;
    return (
      <TableCell>
        { actions.map(action => this.renderAction(item, action)) }
      </TableCell>
    )
  }


  renderAction(item, action) {
    if (action.name === 'show')
      return (
        <Tooltip key={item._id + action.name} title="Просмотреть">
          <IconButton 
            aria-label="Show"
            component={Link}
            to={action.handle('show', item)}>
              <RemoveRedEyeIcon/>
          </IconButton>
        </Tooltip>
      )
    if (action.name === 'edit')
      return (
        <Tooltip key={item._id + action.name} title="Изменить">
          <IconButton 
            aria-label="Edit"
            onClick={(e) => action.handle('edit', item)}>
              <EditIcon/>
          </IconButton>
        </Tooltip>
      )
    if (action.name === 'delete')
      return (
        <Tooltip key={item._id + action.name} title="Удалить">
          <IconButton
            aria-label="Delete"
            onClick={(e) => action.handle('delete', [ item._id ? item._id : item ])}>
              <DeleteIcon/>
          </IconButton>
        </Tooltip>
      )
    if (action.name === 'download') {
      const link = action.handle('download', item);
      if (!link) return;
      return (
        <Tooltip key={item._id + action.name} title="Скачать">
          <IconButton
            aria-label="Download"
            component="a"
            target="_blank"
            href={action.handle('download', item)}>
              <CloudDownloadIcon/>
          </IconButton>
        </Tooltip>
      )
    }
  }


  render() {
    const { classes, fields, handleDialogOpen, colHeadersCustom, rowActions, tableTitle, backButtonLink, data } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar
          numSelected={selected.length}
          handleOpen={handleDialogOpen}
          selectedItems={this.state.selected}
          tableTitle={tableTitle}
          backButtonLink={backButtonLink}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              colHeadersCustom={colHeadersCustom}
            />
            <TableBody>
              {stableSort(data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((n, index) => {
                  const isSelected = this.isSelected(n._id);
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={index}
                      selected={isSelected}>
                        <TableCell
                          padding="checkbox"
                          onClick={event => this.handleClick(event, n._id)}>
                            <Checkbox checked={isSelected} />
                      </TableCell>
                      
                      {this.renderFields(n, fields)}
                      {this.renderActions(n, rowActions)}
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          labelRowsPerPage="Записей на странице:"
          labelDisplayedRows={
            ({ from, to, count }) => `${from} - ${to} из ${count}`
          }
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

PaginationTable.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.array.isRequired,
  handleDialogOpen: PropTypes.func.isRequired,
  colHeadersCustom: PropTypes.array,
  rowActions: PropTypes.array,
  tableTitle: PropTypes.string,
  backButtonLink: PropTypes.string,
};

export default withStyles(styles)(PaginationTable);