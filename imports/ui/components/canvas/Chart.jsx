import React, { Component } from 'react';

import CanvasJSReact from '/imports/ui/components/canvas/canvasjs.react';
import moment from "moment";

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Chart extends Component {

  defaultData = [
    {
      "x": 1483228800000,
      "y": 8561.3
    },
    {
      "x": 1485907200000,
      "y": 8879.6
    },
    {
      "x": 1488326400000,
      "y": 9173.75
    },
    {
      "x": 1491004800000,
      "y": 9304.05
    },
    {
      "x": 1493596800000,
      "y": 9621.25
    },
    {
      "x": 1496275200000,
      "y": 9520.9
    },
    {
      "x": 1498867200000,
      "y": 10077.1
    },
    {
      "x": 1501545600000,
      "y": 9917.9
    },
    {
      "x": 1504224000000,
      "y": 9788.6
    },
    {
      "x": 1506816000000,
      "y": 10335.3
    },
    {
      "x": 1509494400000,
      "y": 10226.55
    },
    {
      "x": 1512086400000,
      "y": 10530.7
    }
  ]


  componentDidMount() {
		var chart = this.chart;
		chart.render();
  }
  
  render() {
    const { data, dataAll, titleY, title } = this.props;

    const dataArray = dataAll 
      ? dataAll 
      : [{
          name: titleY,
          type: "line",
          dataPoints: data ? data : this.defaultData
        }]

    this.options = {
      /** https://canvasjs.com/docs/charts/chart-options/ */
      theme: "light2",
      title: {
        text: title ? title : "Заголовок"
      },
      axisY: {
        title: titleY ? titleY : "Значения",
        // suffix: '',
        includeZero: false
      },
      axisX: {
        labelFormatter: e => {
          return moment(e.value).format('MMM YYYY')
        },
        interval: 1,
        intervalType: "month",
      },
      toolTip: {
        shared: true,
        contentFormatter: e => {
          const date = moment(e.entries[0].dataPoint.x).format('MMM YYYY');
          let content = date + "<br/>";
          for (let i = 0; i < e.entries.length; i++) {
            content += e.entries[i].dataSeries.name + " ";
            content += "<strong>" + e.entries[i].dataPoint.y + "</strong>";
            content += "<br/>";
          }
          return content;
        }
      },
      data: dataArray,
      backgroundColor: 'transparent',
    }
    return (
      <CanvasJSChart
        options = { this.options } 
        onRef={ref => this.chart = ref}
      />
    )
  }

}

export default Chart