import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withMobileDialog from '@material-ui/core/withMobileDialog';

import Dialog from '@material-ui/core/Dialog';

class DialogWindow extends Component {

  render() {
    const { fullScreen, open, handleClose } = this.props;

    return (
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        maxWidth="sm"
        fullWidth={true}
        aria-labelledby="responsive-dialog-title"
      >
        { this.props.children }
      </Dialog>
    )
  }
}

DialogWindow.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default withMobileDialog()(DialogWindow);