import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles'

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

const styles = theme => ({
  mainContainer: {
    width: 'fit-content',
    marginTop: 10,
  },
  card: {
    width: 'fit-content',
    height: '100%',
    marginTop: 10,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  cardAction: {
    flexGrow: 1,
    width: 200,
    height: 140,
    display: 'flex',
    '&:hover $focusHighlight': {
      opacity: 0.3,
    },
  },
  cardActionDelete: {
    '&:hover $cardContentDelete': {
      color: '#fff',
      opacity: 1
    }
  },
  cardActionUpload: {
    '&:hover $cardContentUpload': {
      color: '#000'
    }
  },
  focusHighlight: {},
  cardContent: {
    transition: '.2s',
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    fontSize: 16,
    zIndex: 1,
    top: '50%',
    left: '50%',
    transform: 'translateX(-50%) translateY(-50%)',
  },
  cardContentDelete: {
    opacity: 0,
    color: '#fff',  
  },
  cardContentUpload: {
    color: '#666',
  },
  media: {
    width: 200,
    height: 140,
    backgroundColor: '#eee',
  },
  mediaPlaceholder: {
    width: 200,
    height: 140,
    display: 'block',
    backgroundColor: '#eee',
  },
  backgroundIcon: {
    fontSize: 72,
    color: 'rgba(0,0,0,0.2)',
  },
  fileNameText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    color: 'rgba(0,0,0,0.5)',
    paddingRight: 10,
  },
  icon: {
    marginRight: 5,
  }
});

class FileDownload extends Component {

  state = {

  }


  downloadButtonHandler() {
    const { fileUrl } = this.props;
    setTimeout(() => {
      const response = {
        file: fileUrl,
      };
      // server sent the url to the file!
      // now, let's download:
      window.open(response.file);
      // you could also do:
      // window.location.href = response.file;
    }, 100);
  }


  render() {
    const { classes, fileName = 'document.document' } = this.props;

    return (
      <Card className={ classes.card }>
        <CardActionArea
          title={ "Скачать файл" }
          className={[ classes.cardAction, classes.cardActionDelete ].join(' ')}
          classes={{ focusHighlight: classes.focusHighlight }}
          onClick={() => this.downloadButtonHandler()}>
            <InsertDriveFileIcon
              className={classes.backgroundIcon}/>
            <span className={classes.fileNameText}>
              { fileName }
            </span>
            <CardContent 
              className={[ classes.cardContent, classes.cardContentDelete ].join(' ')}>  
                <SaveAltIcon className={classes.icon}/>
                Скачать
            </CardContent>
        </CardActionArea>
      </Card>
    )
  }
}

export default withStyles(styles, { withTheme: true })(FileDownload)