import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import AddMethodSpaceCategory from '/imports/ui/admin/method-spaces/add';
import DeleteMethodSpaceCategory from '/imports/ui/admin/method-spaces/delete';
import DialogWindow from '/imports/ui/components/DialogWindow';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { MethodSpaceCategories } from '/imports/api/method-space-categories';


const styles = {

};


const colHeaders = [
  { id: 'text', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class MethodSpacesSection extends Component {

  state = {
    documentTitle: 'Методическое пространство',
    methodSpaceCategories: [],

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  };


  fieldsCategories = [
    "name"
  ];


  rowActions = [
    { name: 'show',   handle: (actionName, item) => {
        const link = "/method-space/" + item.shortId
        return link;
      }
    },
    { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
  ];

  
  componentWillMount() {
    document.title = this.state.documentTitle;
    this.tracker = Tracker.autorun(() => {
      const methodSpaceCategories = MethodSpaceCategories.find().fetch();
      this.setState({ methodSpaceCategories: methodSpaceCategories });
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent() {
    const { dialogTypeOpened, dialogSelected, methodSpaceCategories } = this.state;
    if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddMethodSpaceCategory handleClose={this.handleDialogClose} dataSource={methodSpaceCategories}/>;
    // if (dialogTypeOpened === 'show') return <AddMethodSpaceCategory handleClose={this.handleDialogClose} item={dialogSelected} />;
    if (dialogTypeOpened === 'edit') return <AddMethodSpaceCategory handleClose={this.handleDialogClose} item={dialogSelected} dataSource={methodSpaceCategories}/>;
    if (dialogTypeOpened === 'delete') return <DeleteMethodSpaceCategory handleClose={this.handleDialogClose} itemsId={dialogSelected} dataSource={methodSpaceCategories}/>
  }


  render() {
    const { methodSpaceCategories, documentTitle } = this.state;

    return (
      <SidebarWrapper title={documentTitle}>
        <PaginationTable 
          data={ methodSpaceCategories } 
          fields={ this.fieldsCategories }
          handleDialogOpen={ this.handleDialogOpen }
          colHeadersCustom={ colHeaders }
          rowActions={ this.rowActions }
          tableTitle="Список категорий пособий"/>
        <DialogWindow
          open={ this.state.dialogOpened }
          handleClose={ this.handleDialogClose }>
            { this.renderDialogContent() }
        </DialogWindow>
      </SidebarWrapper>
    )
  }
}

MethodSpacesSection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(MethodSpacesSection))