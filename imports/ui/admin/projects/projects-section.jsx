import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import AddProjectCategory from '/imports/ui/admin/projects/add';
import ProjectsChart from '/imports/ui/admin/projects/chart';
import DeleteProjectCategory from '/imports/ui/admin/projects/delete';
import DialogWindow from '/imports/ui/components/DialogWindow';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { ProjectCategories } from '/imports/api/project-categories';


const styles = theme => ({
  
});


const colHeaders = [
  { id: 'text', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class ProjectsSection extends Component {

  state = {
    documentTitle: 'Раздел проектов',
    projectCategories: [],

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  };


  fieldsCategories = [
    "name"
  ];


  rowActions = [
    { name: 'show', handle: (actionName, item) => {
        const link = "/project/" + item.shortId
        return link;
      }
    },
    { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
  ];

  
  componentWillMount() {
    document.title = this.state.documentTitle;
    this.tracker = Tracker.autorun(() => {
      const projectCategories = ProjectCategories.find().fetch();
      this.setState({ projectCategories: projectCategories });
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent() {
    const { dialogTypeOpened, dialogSelected, projectCategories } = this.state;
    if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddProjectCategory handleClose={this.handleDialogClose} dataSource={projectCategories}/>;
    // if (dialogTypeOpened === 'show') return <AddProjectCategory handleClose={this.handleDialogClose} item={dialogSelected} />;
    if (dialogTypeOpened === 'edit') return <AddProjectCategory handleClose={this.handleDialogClose} item={dialogSelected} dataSource={projectCategories}/>;
    if (dialogTypeOpened === 'delete') return <DeleteProjectCategory handleClose={this.handleDialogClose} itemsId={dialogSelected} dataSource={projectCategories}/>
    if (dialogTypeOpened === 'chart') return <ProjectsChart handleClose={this.handleDialogClose}/>
  }


  render() {
    const { projectCategories, documentTitle } = this.state;

    return (
      <SidebarWrapper title={documentTitle}>
        <PaginationTable 
          data={projectCategories} 
          fields={this.fieldsCategories}
          handleDialogOpen={this.handleDialogOpen}
          colHeadersCustom={colHeaders}
          rowActions={this.rowActions}
          tableTitle="Список категорий проектов"/>
        <DialogWindow
          open={this.state.dialogOpened}
          handleClose={this.handleDialogClose}>
            {this.renderDialogContent()}
        </DialogWindow>
      </SidebarWrapper>
    )
  }
}

ProjectsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(ProjectsSection))