import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { ProjectCategories } from '/imports/api/project-categories';


const styles = theme => ({
  
});


class DeleteProjectCategory extends Component {

  state = {
    documentTitle: 'Удаление записи',
    itemsId: [],
  };


  componentWillMount() {
    document.title = this.state.documentTitle;
    const { itemsId } = this.props;
    if (itemsId) this.setState({ itemsId: itemsId });
  }


  handleDelete = () => {
    const { handleClose } = this.props;
    const { itemsId } = this.state;
    if (!itemsId || !itemsId.length) return;
    itemsId.map(itemId => {
      ProjectCategories.remove(itemId);
      this.removeItemFromSource(itemId);
    });
    handleClose();
  }


  removeItemFromSource = itemId => {
    const { dataSource } = this.props;
    for(let i = 0; i < dataSource.length; i++)
      if (dataSource[i]._id === itemId) return dataSource.splice(i, 1);
  }


  renderContentText() {
    const { itemsId } = this.state;
    if (itemsId.length > 1) return "Вы действительно хотите удалить эти записи?"
    return "Вы действительно хотите удалить эту категорию?";
  }


  render() {
    const { handleClose } = this.props;
    const { documentTitle } = this.state;

    return (
      <div>
        <DialogTitle>
          { documentTitle }
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            { this.renderContentText() }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Отмена
          </Button>
          <Button onClick={() => this.handleDelete()} color="primary">
            Удалить
          </Button>
        </DialogActions>
      </div>
    )
  }
}

DeleteProjectCategory.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  itemsId: PropTypes.array.isRequired,
  dataSource: PropTypes.array,
};

export default withStyles(styles, { withTheme: true })(DeleteProjectCategory)