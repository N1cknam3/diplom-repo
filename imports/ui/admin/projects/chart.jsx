import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Chart from '/imports/ui/components/canvas/Chart';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import moment from "moment";

import { ProjectCategories } from '/imports/api/project-categories';


const styles = theme => ({

});


class ProjectsChart extends Component {


  state = {
    
  }


  getAllProjects = () => {
    const categories = ProjectCategories.find().fetch();
    const projects = categories.reduce((res, item) => {
      return res.concat(item.projects)
    }, [])
    return projects;
  }


  generateMonthsData = (count) => {
    const data = [];
    for (let i = count - 1; i >= 0; i--)
      data.push({ 
        date: moment().subtract(i, 'months').startOf('month'), 
        applicationsCount: 0, 
        projectsCount: 0 
      })
    return data;
  }


  sortProjectsByMonths = (projects, monthsData) => {
    return monthsData.map(currMonth => {
      const after = moment(currMonth.date);
      const before = moment(currMonth.date).add(1, 'months');
      const currProjects = projects.filter(comp => moment(comp.endDateAt).isBetween(after, before));
      const currApplications = currProjects.reduce((res, comp) => {
        if (!comp.applications) return res;
        return res.concat(comp.applications)
      }, []);
      currMonth.applicationsCount = currApplications.length;
      currMonth.projectsCount = currProjects.length;
      return currMonth;
    })
  }


  getAllProjectsChartData = () => {
    const countMonths = 6;
    const startDate = moment().subtract(countMonths - 1, 'months').startOf('month');
    const monthsData = this.generateMonthsData(countMonths);
    const projects = this.getAllProjects();
    const includedProjects = projects.filter(item => moment(item.endDateAt).isAfter(startDate));
    const data = this.sortProjectsByMonths(includedProjects, monthsData);
    const result = data.map(item => {
      const value = item.projectsCount 
        ? item.applicationsCount / item.projectsCount 
        : 0;
      return {
        x: moment(item.date).toDate(),
        y: value
      }
    })
    return result;
  }


  render() {
    const { handleClose } = this.props;
    const allProjectsChartData = this.getAllProjectsChartData();
    return (
      <div>
        <DialogContent>
          <Chart 
            title="Показатели проектных конкурсов"
            titleY="Показатель отношения"
            data={allProjectsChartData}/>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Закрыть
          </Button>
        </DialogActions>
      </div>
    )
  }

}

export default withStyles(styles, { withTheme: true })(ProjectsChart)