import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import ImageUpload from '/imports/ui/components/ImageUpload';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import LinearProgress from '@material-ui/core/LinearProgress';
import moment from "moment"
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, InlineDatePicker } from 'material-ui-pickers';
import { storageImageRef } from '/imports/api/firebase';
import TextField from '@material-ui/core/TextField';
import { translit } from 'meteor/akerius:translit';

import { News } from '/imports/api/news';
import "moment/locale/ru";

moment.locale("ru");


const styles = theme => ({
  uploadButton: {
    marginTop: 10,
  }
});


export class AddNews extends Component {

  state = {
    documentTitle: 'Создание новой записи',
    item: {
      title: '',
      text: '',
      shortId: '',
      createdAt: new Date(),
      imageUrl: '',
      fileName: '',
    },
    selectedFile: null,
    savingProcessing: false,
    uploadProgress: 0,
    formErrors: { 
      title: { error: false, text: '' }, 
      shortId: { error: false, text: '' }
    },
    formValid: false
  };


  validatedFields = [
    'title',
    'shortId'
  ];


  componentWillMount() {
    const { item } = this.props;
    const { documentTitle } = this.state;
    document.title = documentTitle;
    if (item) this.setEditItem(item);
  }


  setEditItem = item => {
    this.state.documentTitle = 'Изменение записи';
    document.title = this.state.documentTitle;
    this.setState({ item: { ...item } });
    this.setState({ formValid: true });
  }


  setStateItem = (fieldName, value) => {
    this.setState({
      item: {
        ...this.state.item,
        [fieldName]: value
      }
    })
  }


  setProgressOpacity = () => {
    const { uploadProgress } = this.state;
    return uploadProgress === 0 || uploadProgress === 100 ? 0 : 1;
  }


  handleInputChange = name => event => {
    const value = event.target.value;
    let item = { ...this.state.item, [name]: value };
    if (name === 'title' || name === 'shortId') item.shortId = this.generateShortId(value);
    this.setState({ item: item }, () => this.validateField(name, value));
  }


  validateField = (fieldName, value) => {
    if (this.validatedFields.indexOf(fieldName) === -1) return;
    const { formErrors } = this.state;
    if (fieldName === 'date') return;
    if (fieldName === 'title') formErrors.shortId.error = false;
    formErrors[fieldName].error = !Boolean(value.trim());
    formErrors[fieldName].text = value.trim() ? '' : 'Обязательное поле';
    this.setState({ formErrors: formErrors }, () => this.validateForm());
  }


  validateForm = () => {
    const { formErrors } = this.state;
    const valid = !(formErrors.title.error || formErrors.shortId.error);
    this.setState({ formValid: valid });
  }


  handleDateChange = date => {
    this.setStateItem('createdAt', date.toDate());
  };


  generateShortId = (text, wordsLimit = 5) => {
    return translit(text).split('-').slice(0, wordsLimit).join('-');
  }


  addItemToSource = item => {
    const { dataSource } = this.props;
    item.createdAt = moment(item.createdAt).format('YYYY/MM/DD HH:mm');
    dataSource.push(item);
  }


  updateItemToSource = item => {
    const { dataSource } = this.props;
    var index = dataSource.findIndex(n => n._id == item._id);
    item.createdAt = moment(item.createdAt).format('YYYY/MM/DD HH:mm');
    dataSource[index] = item;
  }


  selectFileHandler = file => {
    this.setState({ selectedFile: file });
    this.setStateItem('fileName', file.name);
    this.setStateItem('imageUrl', URL.createObjectURL(file));
  }


  removeFileHandler = () => {
    this.setStateItem('imageUrl', '');
    this.setState({ selectedFile: null });
  }


  generateFileName = oldName => {
    return new Date().getTime().toString() + '.' + oldName.split('.').pop();
  }


  handleSubmit = e => {
    e.preventDefault(); 
    const { item, selectedFile, formValid } = this.state;
    if (!item || !formValid) return;
    this.setState({ savingProcessing: true });
    if (selectedFile)
      /** Файл выбран, значит его надо загрузить */
      this.processUploadToServer(selectedFile, this.submitItem);
    else if (item.fileName && !item.imageUrl) 
      /** Ссылка на файл удалена, значит по fileName удалим и сам файл */
      this.processDeletingFromServer(item.fileName, this.submitItem);
    else this.submitItem();
  }


  processUploadToServer = (file, afterUploadHandler = () => {}) => {
    const { item } = this.props;
    /** Удаление предыдущего файла, если он есть (по изначальному состоянию) */
    if (item && item.fileName) this.processDeletingFromServer(item.fileName);
    const newFileName = this.generateFileName(file.name);
    this.uploadFileToServer(newFileName, file).then(url => {
      this.setStateItem('imageUrl', url);
      this.setStateItem('fileName', newFileName);
      afterUploadHandler();
    })
  }


  processDeletingFromServer = (fileName, afterDeleteHandler = () => {}) => {
    this.deleteFileFromServer(fileName).then(
      () => {
        this.setStateItem('fileName', '');
        afterDeleteHandler();
      },
      (res) => {
        if (res.code === "storage/object-not-found") {
          /** Файл уже был удален с сервера => удалить упоминание о нем из БД */
          this.setStateItem('fileName', '');
          afterDeleteHandler();
        }
      }
    );
  }


  uploadFileToServer = (fileName, file) => {
    const uploadTask = storageImageRef.child(fileName).put(file);
    uploadTask.on('state_changed', 
      (snapshot) => {
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({ uploadProgress: progress });
      },
      (error) => {
        console.error(error);
      });
    return uploadTask
      .then(() => storageImageRef.child(fileName).getDownloadURL());
  }


  deleteFileFromServer = fileName => {
    return storageImageRef.child(fileName).delete();
  }


  submitItem = () => {
    //  TODO: сделать серверный метод на сохранение, где проверять еще и shortId, что такой уже занят
    const { handleClose } = this.props;
    const { item } = this.state;
    if (!item._id) {
      item._id = News.insert(item);
      if (item._id) this.addItemToSource(item);
    } else {
      const result = News.update(item._id, item);
      if (result) this.updateItemToSource(item);
    }
    this.setState({ savingProcessing: false });
    handleClose();
  }


  renderDatePicker = date => {
    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <InlineDatePicker
          margin="normal"
          label="Дата создания"
          value={date}
          fullWidth
          onChange={this.handleDateChange}
        />
      </MuiPickersUtilsProvider>
    )
  }


  render() {
    const { handleClose } = this.props;
    const { item, documentTitle, uploadProgress, savingProcessing, formValid } = this.state;
    const buttonIsDisabled = savingProcessing || !formValid;
    const { 
      title,
      text,
      shortId,
      createdAt,
      imageUrl,
    } = item;
    const { formErrors } = this.state;
    const titleValid = formErrors.title.error;
    const shortIdValid = formErrors.shortId.error;

    return (
      <form 
        noValidate autoComplete="off" 
        onSubmit={(e) => this.handleSubmit(e)}>
          <DialogContent>
            <DialogContentText>
              { documentTitle }
            </DialogContentText>
              <TextField
                autoFocus
                id="title"
                margin="dense"
                label="Название"
                type="text"
                multiline
                rowsMax="2"
                value={title}
                error={titleValid}
                helperText={formErrors.title.text}
                onChange={this.handleInputChange('title')}
                fullWidth
              />
              { this.renderDatePicker(createdAt) }
              <TextField
                id="shortId"
                margin="dense"
                label="Ссылка"
                type="text"
                multiline
                rowsMax="2"
                value={shortId}
                error={shortIdValid}
                helperText={formErrors.shortId.text}
                onChange={this.handleInputChange('shortId')}
                fullWidth
              />
              <TextField
                id="text"
                margin="dense"
                label="Описание новости"
                type="text"
                multiline
                rows="3"
                rowsMax="6"
                value={text}
                onChange={this.handleInputChange('text')}
                fullWidth
              />
              <ImageUpload
                onSelected={(file) => this.selectFileHandler(file)}
                onRemove={() => this.removeFileHandler()}
                fileUrl={imageUrl}/>
          </DialogContent>
          <LinearProgress
            style={{
              opacity: this.setProgressOpacity()
            }}
            variant="determinate"
            value={uploadProgress}/>
          <DialogActions>
            <Button
              color="primary"
              onClick={handleClose}>
                Отмена
            </Button>
            <Button
              disabled={buttonIsDisabled}
              color="primary"
              type="submit">
                Готово
            </Button>
          </DialogActions>
      </form>
    )
  }
}

AddNews.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  dataSource: PropTypes.array.isRequired,
  item: PropTypes.object,
};

export default withStyles(styles, { withTheme: true })(AddNews)