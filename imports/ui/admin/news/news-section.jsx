import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import AddNews from '/imports/ui/admin/news/add';
import DeleteNews from '/imports/ui/admin/news/delete';
import DialogWindow from '/imports/ui/components/DialogWindow';
import moment from 'moment';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { News } from '/imports/api/news';


const colHeaders = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'createdAt', numeric: false, disablePadding: false, label: 'Дата создания' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


const styles = {
  
}


class NewsSection extends Component {

  state = {
    documentTitle: 'Новостной раздел',
    news: [],

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  };


  fieldsNews = [
    "title",
    "createdAt"
  ];


  rowActions = [
    { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
  ];


  componentWillMount() {
    document.title = this.state.documentTitle;
    this.tracker = Tracker.autorun(() => {
      const news = News.find({}, { sort: { createdAt: -1 } }).fetch();
      this.setState({ news: this.prepareData(news) });
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  prepareData(data) {
    return data.map(({ createdAt, ...other }) => ({
      createdAt: moment(createdAt).format('YYYY/MM/DD HH:mm'),
      ...other
    }))
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent() {
    const { dialogTypeOpened, dialogSelected } = this.state;
    if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddNews handleClose={this.handleDialogClose} dataSource={this.state.news}/>;
    if (dialogTypeOpened === 'edit') return <AddNews handleClose={this.handleDialogClose} item={dialogSelected} dataSource={this.state.news}/>;
    if (dialogTypeOpened === 'delete') return <DeleteNews handleClose={this.handleDialogClose} itemsId={dialogSelected} dataSource={this.state.news}/>
  }


  render() {
    return (
      <SidebarWrapper title={this.state.documentTitle}>
        <PaginationTable 
          data={this.state.news} 
          fields={this.fieldsNews}
          handleDialogOpen={this.handleDialogOpen}
          colHeadersCustom={colHeaders}
          rowActions={this.rowActions}/>
        <DialogWindow
          open={this.state.dialogOpened}
          handleClose={this.handleDialogClose}>
            {this.renderDialogContent()}
        </DialogWindow>
      </SidebarWrapper>
    )
  }
}

NewsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(NewsSection))