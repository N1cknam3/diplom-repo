import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { storageImageRef } from '/imports/api/firebase';

import { News } from '/imports/api/news';


const styles = theme => ({
  
});


class DeleteNews extends Component {

  state = {
    documentTitle: 'Удаление записи',
    itemsId: [],
    dataSource: [],
  };


  componentWillMount() {
    document.title = this.state.documentTitle;
    const { itemsId, dataSource } = this.props;
    if (itemsId) this.setState({ itemsId: itemsId });
    if (dataSource) this.setState({ dataSource: dataSource });
  }


  handleDelete = () => {
    const { handleClose } = this.props;
    const { itemsId } = this.state;
    if (!itemsId || !itemsId.length) return;
    itemsId.map(itemId => {
      const { fileName } = this.getItemFromSource(itemId, this.state.dataSource);
      if (fileName) this.deleteFileFromServer(fileName);
      News.remove(itemId);
      this.removeItemFromSource(itemId, this.state.dataSource);
    });
    handleClose();
  }


  getItemFromSource = (itemId, dataSource) => {
    return dataSource.find(n => n._id == itemId);
  }


  removeItemFromSource = (itemId, dataSource) => {
    return dataSource.find(item => item._id === itemId)
  }


  deleteFileFromServer = fileName => {
    return storageImageRef.child(fileName).delete();
  }


  renderContentText = () => {
    const { itemsId } = this.state;
    if (itemsId.length > 1) return "Вы действительно хотите удалить эти записи?"
    return "Вы действительно хотите удалить эту новость?";
  }


  render() {
    const { handleClose } = this.props;

    return (
      <div>
        <DialogTitle>
          {this.state.documentTitle}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            { this.renderContentText() }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Отмена
          </Button>
          <Button onClick={() => this.handleDelete()} color="primary">
            Удалить
          </Button>
        </DialogActions>
      </div>
    )
  }
}

DeleteNews.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  itemsId: PropTypes.array.isRequired,
  dataSource: PropTypes.array,
};

export default withStyles(styles, { withTheme: true })(DeleteNews)