import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import AddCompetitionCategory from '/imports/ui/admin/competitions/add';
import CompetitionsChart from '/imports/ui/admin/competitions/chart';
import DeleteCompetitionCategory from '/imports/ui/admin/competitions/delete';
import DialogWindow from '/imports/ui/components/DialogWindow';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { CompetitionCategories } from '/imports/api/competition-categories';


const styles = theme => ({
  
});


const colHeaders = [
  { id: 'text', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class CompetitionsSection extends Component {

  state = {
    documentTitle: 'Конкурсный раздел',
    competitionCategories: [],

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  };


  fieldsCategories = [
    "name"
  ];


  rowActions = [
    { name: 'show', handle: (actionName, item) => {
        const link = "/competition/" + item.shortId
        return link;
      }
    },
    { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
  ];

  
  componentWillMount() {
    document.title = this.state.documentTitle;
    this.tracker = Tracker.autorun(() => {
      const competitionCategories = CompetitionCategories.find().fetch();
      this.setState({ competitionCategories: competitionCategories });
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent() {
    const { dialogTypeOpened, dialogSelected, competitionCategories } = this.state;
    if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddCompetitionCategory handleClose={this.handleDialogClose} dataSource={competitionCategories}/>;
    // if (dialogTypeOpened === 'show') return <AddCompetitionCategory handleClose={this.handleDialogClose} item={dialogSelected} />;
    if (dialogTypeOpened === 'edit') return <AddCompetitionCategory handleClose={this.handleDialogClose} item={dialogSelected} dataSource={competitionCategories}/>;
    if (dialogTypeOpened === 'delete') return <DeleteCompetitionCategory handleClose={this.handleDialogClose} itemsId={dialogSelected} dataSource={competitionCategories}/>
    if (dialogTypeOpened === 'chart') return <CompetitionsChart handleClose={this.handleDialogClose}/>
  }


  render() {
    const { competitionCategories, documentTitle } = this.state;

    return (
      <SidebarWrapper title={documentTitle}>
        <PaginationTable 
          data={competitionCategories} 
          fields={this.fieldsCategories}
          handleDialogOpen={this.handleDialogOpen}
          colHeadersCustom={colHeaders}
          rowActions={this.rowActions}
          tableTitle="Список категорий конкурсов"/>
        <DialogWindow
          open={this.state.dialogOpened}
          handleClose={this.handleDialogClose}>
            {this.renderDialogContent()}
        </DialogWindow>
      </SidebarWrapper>
    )
  }
}

CompetitionsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(CompetitionsSection))