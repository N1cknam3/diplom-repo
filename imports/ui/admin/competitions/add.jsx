import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import { translit } from 'meteor/akerius:translit';

import { CompetitionCategories } from '/imports/api/competition-categories';


const styles = theme => ({
  
});


class AddCompetitionCategory extends Component {

  state = {
    documentTitle: 'Создание новой записи',
    item: {
      name: '',
      shortId: '',
      competitions: []
    },
  };


  componentWillMount() {
    const { item } = this.props;
    const { documentTitle } = this.state;
    document.title = documentTitle;
    if (item) this.setItem(item);
  }


  setItem = item => {
    this.state.documentTitle = 'Изменение записи';
    document.title = this.state.documentTitle;
    this.setState({ item: item });
  }


  handleInputChange = name => event => {
    const value = event.target.value;
    let item = { ...this.state.item, [name]: value };
    if (name === 'name' || name === 'shortId') item.shortId = this.generateShortId(value);
    this.setState({ item: item });
  }


  generateShortId = (text, wordsLimit = 5) => {
    return translit(text).split('-').slice(0, wordsLimit).join('-');
  }


  handleSubmit = e => {
    //  TODO: сделать серверный метод на сохранение, где проверять еще и shortId, что такой уже занят
    e.preventDefault();
    const { handleClose } = this.props;
    const { item } = this.state;
    if (!item._id) {
      item._id = CompetitionCategories.insert(item);
      if (item._id) this.addItemToSource(item);
    } else {
      const result = CompetitionCategories.update(item._id, item);
      if (result) this.updateItemToSource(item);
    }
    handleClose();
  }  


  addItemToSource = item => {
    const { dataSource } = this.props;
    dataSource.push(item);
  }
  
  
  updateItemToSource = item => {
    const { dataSource } = this.props;
    var index = dataSource.find(n => n._id == item._id);
    dataSource[index] = item;
  }


  render() {
    const { handleClose } = this.props;
    const { item, documentTitle } = this.state;
    const {
      name = '',
      shortId = '',
    } = item;

    return (
      <div>
        <form 
          noValidate 
          autoComplete="off" 
          onSubmit={(e) => this.handleSubmit(e)}>
            <DialogContent>
              <DialogContentText>
                { documentTitle }
              </DialogContentText>
                <TextField
                  autoFocus
                  id="text"
                  margin="dense"
                  label="Название"
                  type="text"
                  multiline
                  rowsMax="4"
                  value={name}
                  onChange={this.handleInputChange('name')}
                  fullWidth
                />
                <TextField
                  id="shortId"
                  margin="dense"
                  label="Ссылка"
                  type="text"
                  multiline
                  rowsMax="4"
                  value={shortId}
                  onChange={this.handleInputChange('shortId')}
                  fullWidth
                />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Отмена
              </Button>
              <Button color="primary" type="submit">
                Готово
              </Button>
            </DialogActions>
        </form>
      </div>
    )
  }
}

AddCompetitionCategory.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  item: PropTypes.object,
  dataSource: PropTypes.array,
};

export default withStyles(styles, { withTheme: true })(AddCompetitionCategory)