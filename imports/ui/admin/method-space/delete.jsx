import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { MethodSpaceCategories } from '/imports/api/method-space-categories';


const styles = theme => ({
  
});


class DeleteMethodSpace extends Component {

  state = {
    documentTitle: 'Удаление записи',
    items: [],
  };


  componentWillMount() {
    const { items } = this.props;
    const { documentTitle } = this.state;
    document.title = documentTitle;
    if (items) this.setState({ items: items });
  }


  handleDelete = () => {
    const { items } = this.state;
    const { handleClose } = this.props;
    if (!items || !items.length) return;
    items.map(item => {
      this.updateMaterial(item);
    });
    handleClose();
  }


  updateMaterial(item) {
    const { category } = this.props;
    const result = MethodSpaceCategories.update(category._id, {
      $pull: {
        materials: { 
          shortId: item.shortId 
        }
      }
    });
    return result;
  }


  renderContentText() {
    const { items } = this.state;
    if (items.length > 1) return "Вы действительно хотите удалить эти записи?"
    return "Вы действительно хотите удалить эту категорию пособий?";
  }


  render() {
    const { documentTitle } = this.state;
    const { handleClose } = this.props;

    return (
      <div>
        <DialogTitle>
          { documentTitle }
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            { this.renderContentText() }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Отмена
          </Button>
          <Button onClick={() => this.handleDelete()} color="primary">
            Удалить
          </Button>
        </DialogActions>
      </div>
    )
  }
}

DeleteMethodSpace.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  category: PropTypes.object,
};

export default withStyles(styles, { withTheme: true })(DeleteMethodSpace)