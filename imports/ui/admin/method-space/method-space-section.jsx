import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import AddMethodSpace from '/imports/ui/admin/method-space/add'
import DeleteMethodSpace from '/imports/ui/admin/method-space/delete'
import DialogWindow from '/imports/ui/components/DialogWindow'
import PaginationTable from '/imports/ui/components/PaginationTable'
import PropTypes from 'prop-types'
import SidebarWrapper from '/imports/ui/components/SidebarWrapper'

import { MethodSpaceCategories } from '/imports/api/method-space-categories';


const styles = {

};


const colHeaders = [
  { id: 'text', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class MethodSpaceSection extends Component {

  state = {
    documentTitle: 'Методическое пространство',
    currentCategory: null,

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  };


  fieldsCategories = [
    "title"
  ];


  rowActions = [
    { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
  ];


  componentWillMount() {
    const { documentTitle } = this.state;
    const { categoryId } = this.props.match.params;
    document.title = documentTitle;
    this.tracker = Tracker.autorun(() => {
      const currentCategory = MethodSpaceCategories.findOne({ shortId: categoryId });
      this.setState({currentCategory: currentCategory});
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  getCompetiotionIndex = item => {
    const { currentCategory: { materials } } = this.state;
    return materials.findIndex(i => i.shortId === item.shortId);
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent = () => {
    const { dialogTypeOpened, dialogSelected, currentCategory } = this.state;
    if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddMethodSpace handleClose={this.handleDialogClose} category={currentCategory}/>;
    // if (dialogTypeOpened === 'show') return <AddMethodSpace handleClose={this.handleDialogClose} item={dialogSelected} />;
    if (dialogTypeOpened === 'edit') return <AddMethodSpace handleClose={this.handleDialogClose} item={dialogSelected} category={currentCategory} materialIndex={this.getCompetiotionIndex(dialogSelected)}/>;
    if (dialogTypeOpened === 'delete') return <DeleteMethodSpace handleClose={this.handleDialogClose} items={dialogSelected} category={currentCategory}/>
  }


  render() {
    const { dialogOpened, documentTitle } = this.state;
    const { currentCategory: { materials } } = this.state;

    return (
      <SidebarWrapper 
        title={ documentTitle }>
          <PaginationTable 
            data={ materials } 
            fields={ this.fieldsCategories }
            handleDialogOpen={ this.handleDialogOpen }
            colHeadersCustom={ colHeaders }
            rowActions={ this.rowActions }
            tableTitle="Список материалов"
            backButtonLink="/method-spaces"/>
          <DialogWindow
            open={ dialogOpened }
            handleClose={ this.handleDialogClose }>
              { this.renderDialogContent() }
          </DialogWindow>
      </SidebarWrapper>
    )
  }
}

MethodSpaceSection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(MethodSpaceSection))