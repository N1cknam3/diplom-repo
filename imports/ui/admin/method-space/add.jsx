import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import FileUpload from '/imports/ui/components/FileUpload';
import LinearProgress from '@material-ui/core/LinearProgress';
import { storageMethodSpaceMaterialsRef } from '/imports/api/firebase';
import TextField from '@material-ui/core/TextField';
import { translit } from 'meteor/akerius:translit';

import { MethodSpaceCategories } from '/imports/api/method-space-categories';


const styles = theme => ({
  
});


class AddMethodSpace extends Component {

  state = {
    documentTitle: 'Создание новой записи',
    item: {
      title: '',
      shortId: '',
      text: '',
      fileName: '',
      fileUrl: '',
    },
    selectedFile: null,
    savingProcessing: false,
    uploadProgress: 0,
  };


  componentWillMount() {
    const { item } = this.props;
    const { documentTitle } = this.state;
    document.title = documentTitle;
    if (item) this.setEditItem(item);
  }


  setEditItem = item => {
    this.state.documentTitle = 'Изменение записи';
    document.title = this.state.documentTitle;
    this.setState({ item: item });
  }


  setStateItem = (fieldName, value) => {
    this.setState({
      item: {
        ...this.state.item,
        [fieldName]: value
      }
    })
  }


  setProgressOpacity = () => {
    const { uploadProgress } = this.state;
    return uploadProgress === 0 || uploadProgress === 100 ? 0 : 1;
  }


  handleInputChange = name => event => {
    const value = event.target.value;
    let item = { ...this.state.item, [name]: value };
    if (name === 'title' || name === 'shortId') item.shortId = this.generateShortId(value);
    this.setState({ item: item });
  }


  generateShortId = (text, wordsLimit = 5) => {
    return translit(text).split('-').slice(0, wordsLimit).join('-');
  }


  addMaterialToSource = item => {
    const { category } = this.props;
    const result = MethodSpaceCategories.update(category._id, {
      $push: {
        materials: item
      }
    });
    category.materials.push(item);
    return result;
  }


  updateMaterialToSource = item => {
    const { category, materialIndex } = this.props;
    const result = MethodSpaceCategories.update(category._id, {
      $set: {
        ['materials.' + materialIndex]: item
      }
    });
    category.materials[materialIndex] = item;
    return result;
  }


  selectFileHandler = file => {
    this.setState({ selectedFile: file });
    this.setStateItem('fileUrl', URL.createObjectURL(file));
  }


  removeFileHandler = () => {
    this.setStateItem('fileUrl', '');
    this.setState({ selectedFile: null });
  }


  generateFileName = oldName => {
    return new Date().getTime().toString() + '.' + oldName.split('.').pop();
  }


  handleSubmit = e => {
    e.preventDefault();
    const { item, selectedFile } = this.state;
    if (!item) return;
    this.setState({ savingProcessing: true });
    if (selectedFile)
      /** Файл выбран, значит его надо загрузить */
      this.processUploadToServer(selectedFile, this.submitItem);
    else if (item.fileName && !item.fileUrl) 
      /** Ссылка на файл удалена, значит по fileName удалим и сам файл */
      this.processDeletingFromServer(item.fileName, this.submitItem);
    else this.submitItem();
  }


  processUploadToServer = (file, afterUploadHandler = () => {}) => {
    const { item } = this.props;
    /** Удаление предыдущего файла, если он есть (по изначальному состоянию) */
    if (item && item.fileName) this.processDeletingFromServer(item.fileName);
    const newFileName = this.generateFileName(file.name);
    this.uploadFileToServer(newFileName, file).then(url => {
      this.setStateItem('fileUrl', url);
      this.setStateItem('fileName', newFileName);
      afterUploadHandler();
    })
  }


  processDeletingFromServer = (fileName, afterDeleteHandler = () => {}) => {
    this.deleteFileFromServer(fileName).then(
      () => {
        this.setStateItem('fileName', '');
        afterDeleteHandler();
      },
      (res) => {
        if (res.code === "storage/object-not-found") {
          /** Файл уже был удален с сервера => удалить упоминание о нем из БД */
          this.setStateItem('fileName', '');
          afterDeleteHandler();
        }
      }
    );
  }


  uploadFileToServer = (fileName, file) => {
    const uploadTask = storageMethodSpaceMaterialsRef.child(fileName).put(file);
    uploadTask.on('state_changed', 
      (snapshot) => {
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({ uploadProgress: progress });
      },
      (error) => {
        console.error(error);
      });
    return uploadTask
      .then(() => storageMethodSpaceMaterialsRef.child(fileName).getDownloadURL());
  }


  deleteFileFromServer = fileName => {
    return storageMethodSpaceMaterialsRef.child(fileName).delete();
  }


  submitItem = () => {
    //  TODO: сделать серверный метод на сохранение, где проверять еще и shortId, что такой уже занят
    const { handleClose, materialIndex = -1 } = this.props;
    const { item } = this.state;
    if (materialIndex === -1) this.addMaterialToSource(item);
    else this.updateMaterialToSource(item);
    this.setState({ savingProcessing: false });
    handleClose();
  }


  render() {
    const { handleClose } = this.props;
    const { item, documentTitle, selectedFile, uploadProgress, savingProcessing } = this.state;
    const buttonIsDisabled = savingProcessing;
    const {
      title = '',
      shortId = '',
      text = '',
    } = item;
    const fileName = selectedFile ? selectedFile.name
      : item.fileUrl && item.fileName ? item.fileName
      : '';

    return (
      <div>
        <form
          noValidate
          autoComplete="off"
          onSubmit={ e => this.handleSubmit(e) }>
            <DialogContent>
              <DialogContentText>
                { documentTitle }
              </DialogContentText>
                <TextField
                  autoFocus
                  id="title"
                  margin="dense"
                  label="Название"
                  type="text"
                  multiline
                  rowsMax="4"
                  value={ title }
                  onChange={ this.handleInputChange('title') }
                  fullWidth/>
                <TextField
                  id="shortId"
                  margin="dense"
                  label="Ссылка"
                  type="text"
                  multiline
                  rowsMax="4"
                  value={ shortId }
                  onChange={ this.handleInputChange('shortId') }
                  fullWidth/>
                <TextField
                  id="text"
                  margin="dense"
                  label="Описание метод.материала"
                  type="text"
                  multiline
                  rows="3"
                  rowsMax="6"
                  value={ text }
                  onChange={ this.handleInputChange('text') }
                  fullWidth/>
                <FileUpload
                  onSelected={ file => this.selectFileHandler(file) }
                  onRemove={ () => this.removeFileHandler() }
                  fileName={ fileName }/>
            </DialogContent>
            <LinearProgress
              style={{
                opacity: this.setProgressOpacity()
              }}
              variant="determinate"
              value={uploadProgress}/>
            <DialogActions>
              <Button
                disabled={buttonIsDisabled}
                color="primary"
                onClick={handleClose}>
                  Отмена
              </Button>
              <Button
                disabled={buttonIsDisabled}
                color="primary"
                type="submit">
                  Готово
              </Button>
            </DialogActions>
        </form>
      </div>
    )
  }
}

AddMethodSpace.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  item: PropTypes.object,
  category: PropTypes.object,
  materialIndex: PropTypes.number,
};

export default withStyles(styles, { withTheme: true })(AddMethodSpace)