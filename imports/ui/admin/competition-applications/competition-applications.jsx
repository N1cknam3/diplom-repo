import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import CompetitionApplicationsChart from '/imports/ui/admin/competition-applications/chart';
import DialogWindow from '/imports/ui/components/DialogWindow';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { CompetitionCategories } from '/imports/api/competition-categories';


const styles = {

};


const colHeaders = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class CompetitionApplicationsSection extends Component {

  state = {
    documentTitle: 'Заявки на конкурс',
    currentCategory: null,

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  }


  fieldsCategories = [
    "title"
  ];


  rowActions = [
    // { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    // { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'download', handle: (actionName, item) => {
        return item.fileUrl;
      }
    },
  ];


  componentWillMount() {
    const { documentTitle } = this.state;
    const { itemId } = this.props.match.params;
    document.title = documentTitle;
    this.tracker = Tracker.autorun(() => {
      const currentCategory = CompetitionCategories.findOne({
        'competitions': {
          $elemMatch: {
            shortId: itemId
          }
        }
      });
      this.setState({ currentCategory: currentCategory });
    });
  }


  // prepareData = category => {
  //   category.competitions = category.competitions.map(({ applications, ...other}) => ({
  //     ...other,
  //     applications: !applications ? [] : applications.map(({ fileUrl, ...other }) => ({
  //       fileUrl: fileUrl ? `<a href="${fileUrl}">СКАЧАТЬ</a>` : '',
  //       ...other
  //     }))
  //   }))
  //   return category;
  // }


  getCompetitionIndex = shortId => {
    const { currentCategory: { competitions } } = this.state;
    return competitions.findIndex(i => i.shortId === shortId);
  }


  getCompetition = () => {
    const { currentCategory: { competitions } } = this.state;
    const { itemId } = this.props.match.params;
    const index = this.getCompetitionIndex(itemId);
    return competitions[index];
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent = () => {
    const { dialogTypeOpened, currentCategory } = this.state;
    const { itemId } = this.props.match.params;
    const index = this.getCompetitionIndex(itemId);
    // if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddCompetition handleClose={this.handleDialogClose} category={currentCategory}/>;
    // // if (dialogTypeOpened === 'show') return <AddCompetition handleClose={this.handleDialogClose} item={dialogSelected} />;
    // if (dialogTypeOpened === 'edit') return <AddCompetition handleClose={this.handleDialogClose} item={dialogSelected} category={currentCategory} competitionIndex={this.getCompetiotionIndex(dialogSelected)}/>;
    // if (dialogTypeOpened === 'delete') return <DeleteCompetition handleClose={this.handleDialogClose} items={dialogSelected} category={currentCategory}/>
    if (dialogTypeOpened === 'chart') return <CompetitionApplicationsChart handleClose={this.handleDialogClose} category={currentCategory} competitionIndex={index}/>
    return '';
  }


  render() {
    const { dialogOpened, documentTitle, currentCategory } = this.state;
    const competition = this.getCompetition();
    const applications = competition.applications ? competition.applications : [];
    const backButtonLink = '/competition/' + currentCategory.shortId;

    return (
      <SidebarWrapper 
        title={ documentTitle }>
          <PaginationTable 
            data={ applications } 
            fields={ this.fieldsCategories }
            handleDialogOpen={ this.handleDialogOpen }
            colHeadersCustom={ colHeaders }
            rowActions={ this.rowActions }
            tableTitle="Список заявок"
            backButtonLink={ backButtonLink }/>
          <DialogWindow
            open={ dialogOpened }
            handleClose={ this.handleDialogClose }>
              { this.renderDialogContent() }
          </DialogWindow>
      </SidebarWrapper>
    )
  }
}

CompetitionApplicationsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  competition: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(CompetitionApplicationsSection))