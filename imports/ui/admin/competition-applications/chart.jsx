import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Chart from '/imports/ui/components/canvas/Chart';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import moment from "moment";


const styles = theme => ({

});


class CompetitionApplicationsChart extends Component {


  state = {
    
  }


  getCompetition = () => {
    const { category, competitionIndex } = this.props;
    const competition = category.competitions ? category.competitions[competitionIndex] : [];
    return [ competition ];
  }


  generateMonthsData = (count) => {
    const data = [];
    for (let i = count - 1; i >= 0; i--)
      data.push({ 
        date: moment().subtract(i, 'months').startOf('month'), 
        applicationsCount: 0, 
        competitionsCount: 0 
      })
    return data;
  }


  sortCompetitionsByMonths = (competitions, monthsData) => {
    return monthsData.map(currMonth => {
      const after = moment(currMonth.date);
      const before = moment(currMonth.date).add(1, 'months');
      const currCompetitions = competitions.filter(comp => moment(comp.endDateAt).isBetween(after, before));
      const currApplications = currCompetitions.reduce((res, comp) => {
        if (!comp.applications) return res;
        return res.concat(comp.applications)
      }, []);
      currMonth.applicationsCount = currApplications.length;
      currMonth.competitionsCount = currCompetitions.length;
      return currMonth;
    })
  }


  getAllCompetitionsChartData = () => {
    const countMonths = 6;
    const startDate = moment().subtract(countMonths - 1, 'months').startOf('month');
    const monthsData = this.generateMonthsData(countMonths);
    const competitions = this.getCompetition();
    const includedCompetitions = competitions.filter(item => moment(item.endDateAt).isAfter(startDate));
    const data = this.sortCompetitionsByMonths(includedCompetitions, monthsData);
    const result = [
      { name: "Кол-во заявок", type: "line", dataPoints: [], showInLegend: true, },
      { name: "Кол-во конкурсов", type: "line", dataPoints: [], showInLegend: true, },
    ];
    result[0].dataPoints = data.map(item => {
      return {
        x: moment(item.date).toDate(),
        y: item.applicationsCount
      }
    })
    result[1].dataPoints = data.map(item => {
      return {
        x: moment(item.date).toDate(),
        y: item.competitionsCount
      }
    })
    return result;
  }


  render() {
    const { handleClose } = this.props;
    const allCompetitionsChartData = this.getAllCompetitionsChartData();
    return (
      <div>
        <DialogContent>
          <Chart 
            title="Показатели конкурсов"
            titleY="Показатель отношения"
            dataAll={allCompetitionsChartData}/>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Закрыть
          </Button>
        </DialogActions>
      </div>
    )
  }

}

export default withStyles(styles, { withTheme: true })(CompetitionApplicationsChart)