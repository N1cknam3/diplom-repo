import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import AddCompetition from '/imports/ui/admin/competition/add';
import CompetitionCategoryChart from '/imports/ui/admin/competition/chart';
import DeleteCompetition from '/imports/ui/admin/competition/delete';
import DialogWindow from '/imports/ui/components/DialogWindow';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { CompetitionCategories } from '/imports/api/competition-categories';


const styles = {

};


const colHeaders = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class CompetitionSection extends Component {

  state = {
    documentTitle: 'Конкурсный раздел',
    currentCategory: null,

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  };


  fieldsCategories = [
    "title"
  ];


  rowActions = [
    { name: 'show', handle: (actionName, item) => {
        const link = "/competition-applications/" + item.shortId
        return link;
      }
    },
    { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
  ];


  componentWillMount() {
    const { documentTitle } = this.state;
    const { categoryId } = this.props.match.params;
    document.title = documentTitle;
    this.tracker = Tracker.autorun(() => {
      const currentCategory = CompetitionCategories.findOne({ shortId: categoryId });
      this.setState({currentCategory: currentCategory});
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  getCompetitionIndex = item => {
    const { currentCategory: { competitions } } = this.state;
    return competitions.findIndex(i => i.shortId === item.shortId);
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent = () => {
    const { dialogTypeOpened, dialogSelected, currentCategory } = this.state;
    if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddCompetition handleClose={this.handleDialogClose} category={currentCategory}/>;
    // if (dialogTypeOpened === 'show') return <AddCompetition handleClose={this.handleDialogClose} item={dialogSelected} />;
    if (dialogTypeOpened === 'edit') return <AddCompetition handleClose={this.handleDialogClose} item={dialogSelected} category={currentCategory} competitionIndex={this.getCompetitionIndex(dialogSelected)}/>;
    if (dialogTypeOpened === 'delete') return <DeleteCompetition handleClose={this.handleDialogClose} items={dialogSelected} category={currentCategory}/>
    if (dialogTypeOpened === 'chart') return <CompetitionCategoryChart handleClose={this.handleDialogClose} category={currentCategory}/>
  }


  render() {
    const { dialogOpened, documentTitle } = this.state;
    const { currentCategory: { competitions } } = this.state;

    return (
      <SidebarWrapper 
        title={ documentTitle }>
          <PaginationTable 
            data={ competitions } 
            fields={ this.fieldsCategories }
            handleDialogOpen={ this.handleDialogOpen }
            colHeadersCustom={ colHeaders }
            rowActions={ this.rowActions }
            tableTitle="Список конкурсов"
            backButtonLink="/competitions"/>
          <DialogWindow
            open={ dialogOpened }
            handleClose={ this.handleDialogClose }>
              { this.renderDialogContent() }
          </DialogWindow>
      </SidebarWrapper>
    )
  }
}

CompetitionSection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(CompetitionSection))