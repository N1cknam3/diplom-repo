import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import moment from "moment"
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, InlineDatePicker } from 'material-ui-pickers';
import TextField from '@material-ui/core/TextField';
import { translit } from 'meteor/akerius:translit';

import { CompetitionCategories } from '/imports/api/competition-categories';
import "moment/locale/ru";

moment.locale("ru");


const styles = theme => ({
  
});


class AddCompetition extends Component {

  state = {
    documentTitle: 'Создание новой записи',
    item: {
      title: '',
      shortId: '',
      createdAt: new Date(),
      endDateAt: new Date(),
      text: '',
    },
  };


  componentWillMount() {
    const { item } = this.props;
    const { documentTitle } = this.state;
    document.title = documentTitle;
    if (item) this.setEditItem(item);
  }


  setEditItem = item => {
    this.state.documentTitle = 'Изменение записи';
    document.title = this.state.documentTitle;
    this.setState({ item: item });
  }


  setStateItem = (fieldName, value) => {
    this.setState({
      item: {
        ...this.state.item,
        [fieldName]: value
      }
    })
  }


  handleInputChange = name => event => {
    const value = event.target.value;
    let item = { ...this.state.item, [name]: value };
    if (name === 'title' || name === 'shortId') item.shortId = this.generateShortId(value);
    this.setState({ item: item });
  }


  handleDateChange = date => {
    this.setStateItem('endDateAt', date.toDate());
  };


  generateShortId = (text, wordsLimit = 5) => {
    return translit(text).split('-').slice(0, wordsLimit).join('-');
  }


  handleSubmit = e => {
    //  TODO: сделать серверный метод на сохранение, где проверять еще и shortId, что такой уже занят
    const { handleClose, competitionIndex = -1 } = this.props;
    const { item } = this.state;
    e.preventDefault();
    if (!item) return;
    if (competitionIndex === -1) this.addCompetition(item);
    else this.updateCompetition(item);
    handleClose();
  }


  addCompetition = item => {
    const { category } = this.props;
    item.createdAt = new Date();
    const result = CompetitionCategories.update(category._id, {
      $push: {
        competitions: item
      }
    });
    category.competitions.push(item);
    return result;
  }


  updateCompetition = item => {
    const { category, competitionIndex } = this.props;
    const result = CompetitionCategories.update(category._id, {
      $set: {
        ['competitions.' + competitionIndex]: item
      }
    });
    category.competitions[competitionIndex] = item;
    return result;
  }


  renderDatePicker = date => {
    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <InlineDatePicker
          margin="normal"
          label="Дата окончания конкурса"
          value={date}
          fullWidth
          onChange={this.handleDateChange}
        />
      </MuiPickersUtilsProvider>
    )
  }


  render() {
    const { handleClose } = this.props;
    const { item, documentTitle } = this.state;
    const {
      title = '',
      shortId = '',
      endDateAt = new Date(),
      text = '',
    } = item;

    return (
      <div>
        <form
          noValidate
          autoComplete="off"
          onSubmit={ e => this.handleSubmit(e) }>
            <DialogContent>
              <DialogContentText>
                { documentTitle }
              </DialogContentText>
                <TextField
                  autoFocus
                  id="title"
                  margin="dense"
                  label="Название"
                  type="text"
                  multiline
                  rowsMax="4"
                  value={ title }
                  onChange={ this.handleInputChange('title') }
                  fullWidth/>
                <TextField
                  id="shortId"
                  margin="dense"
                  label="Ссылка"
                  type="text"
                  multiline
                  rowsMax="4"
                  value={ shortId }
                  onChange={ this.handleInputChange('shortId') }
                  fullWidth/>
                { this.renderDatePicker(endDateAt) }
                <TextField
                  id="text"
                  margin="dense"
                  label="Описание конкурса"
                  type="text"
                  multiline
                  rows="3"
                  rowsMax="6"
                  value={ text }
                  onChange={ this.handleInputChange('text') }
                  fullWidth/>
            </DialogContent>
            <DialogActions>
              <Button onClick={ handleClose } color="primary">
                Отмена
              </Button>
              <Button color="primary" type="submit">
                Готово
              </Button>
            </DialogActions>
        </form>
      </div>
    )
  }
}

AddCompetition.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  item: PropTypes.object,
  category: PropTypes.object,
  competitionIndex: PropTypes.number,
};

export default withStyles(styles, { withTheme: true })(AddCompetition)