import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { CompetitionCategories } from '/imports/api/competition-categories';


const styles = theme => ({
  
});


class DeleteCompetition extends Component {

  state = {
    documentTitle: 'Удаление записи',
    items: [],
  };


  componentWillMount() {
    const { items } = this.props;
    const { documentTitle } = this.state;
    document.title = documentTitle;
    if (items) this.setState({ items: items });
  }


  handleDelete = () => {
    const { items } = this.state;
    const { handleClose } = this.props;
    if (!items || !items.length) return;
    items.map(item => {
      this.updateCompetition(item);
    });
    handleClose();
  }


  updateCompetition(item) {
    const { category } = this.props;
    const result = CompetitionCategories.update(category._id, {
      $pull: {
        competitions: { 
          shortId: item.shortId 
        }
      }
    });
    return result;
  }


  renderContentText() {
    const { items: itemsId } = this.state;
    if (itemsId.length > 1) return "Вы действительно хотите удалить эти записи?"
    return "Вы действительно хотите удалить эту категорию?";
  }


  render() {
    const { documentTitle } = this.state;
    const { handleClose } = this.props;

    return (
      <div>
        <DialogTitle>
          { documentTitle }
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            { this.renderContentText() }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Отмена
          </Button>
          <Button onClick={() => this.handleDelete()} color="primary">
            Удалить
          </Button>
        </DialogActions>
      </div>
    )
  }
}

DeleteCompetition.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  category: PropTypes.object,
};

export default withStyles(styles, { withTheme: true })(DeleteCompetition)