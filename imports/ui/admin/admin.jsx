import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({

});


class Admin extends Component {

  state = {
    documentTitle: 'Кабинет администратора'
  };

  componentWillMount() {
    document.title = this.state.documentTitle;
  }


  render() {
    return (
      <SidebarWrapper title={this.state.documentTitle}>
        <Typography paragraph>
          Это кабинет администратора. Добро пожаловать!
        </Typography>
        <Typography paragraph>
          Выберите нужный раздел и начните работу.
        </Typography>
      </SidebarWrapper>
    )
  }
}

Admin.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(Admin))