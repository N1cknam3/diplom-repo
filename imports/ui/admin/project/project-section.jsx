import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import AddProject from '/imports/ui/admin/project/add';
import ProjectCategoryChart from '/imports/ui/admin/project/chart';
import DeleteProject from '/imports/ui/admin/project/delete';
import DialogWindow from '/imports/ui/components/DialogWindow';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { ProjectCategories } from '/imports/api/project-categories';


const styles = {

};


const colHeaders = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class ProjectSection extends Component {

  state = {
    documentTitle: 'Раздел проектов',
    currentCategory: null,
    // projects: [],

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  };


  fieldsCategories = [
    "title"
  ];


  rowActions = [
    { name: 'show', handle: (actionName, item) => {
        const link = "/project-applications/" + item.shortId
        return link;
      }
    },
    { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
  ];


  componentWillMount() {
    const { documentTitle } = this.state;
    const { categoryId } = this.props.match.params;
    document.title = documentTitle;
    this.tracker = Tracker.autorun(() => {
      const currentCategory = ProjectCategories.findOne({ shortId: categoryId });
      this.setState({currentCategory: currentCategory});
      // this.setState({projects: currentCategory.projects});
    });
  }


  componentWillUnmount() {
    this.tracker.stop();
  }


  getProjectIndex = item => {
    const { currentCategory: { projects } } = this.state;
    return projects.findIndex(i => i.shortId === item.shortId);
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent = () => {
    const { dialogTypeOpened, dialogSelected, currentCategory } = this.state;
    if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddProject handleClose={this.handleDialogClose} category={currentCategory}/>;
    // if (dialogTypeOpened === 'show') return <AddProject handleClose={this.handleDialogClose} item={dialogSelected} />;
    if (dialogTypeOpened === 'edit') return <AddProject handleClose={this.handleDialogClose} item={dialogSelected} category={currentCategory} projectIndex={this.getProjectIndex(dialogSelected)}/>;
    if (dialogTypeOpened === 'delete') return <DeleteProject handleClose={this.handleDialogClose} items={dialogSelected} category={currentCategory}/>
    if (dialogTypeOpened === 'chart') return <ProjectCategoryChart handleClose={this.handleDialogClose} category={currentCategory}/>
  }


  render() {
    const { documentTitle, dialogOpened } = this.state;
    const { currentCategory: { projects } } = this.state;

    return (
      <SidebarWrapper
        title={ documentTitle }>
          <PaginationTable 
            data={ projects } 
            fields={ this.fieldsCategories }
            handleDialogOpen={ this.handleDialogOpen }
            colHeadersCustom={ colHeaders }
            rowActions={ this.rowActions }
            tableTitle="Список проектов"
            backButtonLink="/projects"/>
          <DialogWindow
            open={ dialogOpened }
            handleClose={ this.handleDialogClose }>
              { this.renderDialogContent() }
          </DialogWindow>
      </SidebarWrapper>
    )
  }
}

ProjectSection.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(ProjectSection))