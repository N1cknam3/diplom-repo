import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Chart from '/imports/ui/components/canvas/Chart';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import moment from "moment";


const styles = theme => ({

});


class ProjectApplicationsChart extends Component {


  state = {
    
  }


  getProject = () => {
    const { category, projectIndex } = this.props;
    const project = category.projects ? category.projects[projectIndex] : [];
    return [ project ];
  }


  generateMonthsData = (count) => {
    const data = [];
    for (let i = count - 1; i >= 0; i--)
      data.push({ 
        date: moment().subtract(i, 'months').startOf('month'), 
        applicationsCount: 0, 
        competitionsCount: 0 
      })
    return data;
  }


  sortProjectsByMonths = (projects, monthsData) => {
    return monthsData.map(currMonth => {
      const after = moment(currMonth.date);
      const before = moment(currMonth.date).add(1, 'months');
      const currProjects = projects.filter(project => moment(project.endDateAt).isBetween(after, before));
      const currApplications = currProjects.reduce((res, project) => {
        if (!project.applications) return res;
        return res.concat(project.applications)
      }, []);
      currMonth.applicationsCount = currApplications.length;
      currMonth.projectsCount = currProjects.length;
      return currMonth;
    })
  }


  getAllProjectsChartData = () => {
    const countMonths = 6;
    const startDate = moment().subtract(countMonths - 1, 'months').startOf('month');
    const monthsData = this.generateMonthsData(countMonths);
    const projects = this.getProject();
    const includedProjects = projects.filter(item => moment(item.endDateAt).isAfter(startDate));
    const data = this.sortProjectsByMonths(includedProjects, monthsData);
    const result = [
      { name: "Кол-во заявок", type: "line", dataPoints: [], showInLegend: true, },
      { name: "Кол-во конкурсов", type: "line", dataPoints: [], showInLegend: true, },
    ];
    result[0].dataPoints = data.map(item => {
      return {
        x: moment(item.date).toDate(),
        y: item.applicationsCount
      }
    })
    result[1].dataPoints = data.map(item => {
      return {
        x: moment(item.date).toDate(),
        y: item.projectsCount
      }
    })
    return result;
  }


  render() {
    const { handleClose } = this.props;
    const allProjectsChartData = this.getAllProjectsChartData();
    return (
      <div>
        <DialogContent>
          <Chart 
            title="Показатели проектных конкурсов"
            titleY="Показатель отношения"
            dataAll={allProjectsChartData}/>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Закрыть
          </Button>
        </DialogActions>
      </div>
    )
  }

}

export default withStyles(styles, { withTheme: true })(ProjectApplicationsChart)