import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import ProjectApplicationsChart from '/imports/ui/admin/project-applications/chart';
import DialogWindow from '/imports/ui/components/DialogWindow';
import PaginationTable from '/imports/ui/components/PaginationTable';
import PropTypes from 'prop-types';
import SidebarWrapper from '/imports/ui/components/SidebarWrapper';

import { ProjectCategories } from '/imports/api/project-categories';


const styles = {

};


const colHeaders = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Заголовок' },
  { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
];


class ProjectApplicationsSection extends Component {

  state = {
    documentTitle: 'Заявки на конкурс проектов',
    currentCategory: null,

    dialogOpened: false,
    dialogTypeOpened: 'add',  /* Тип диалогового окна */
    dialogSelected: null,     /* Выбранные элементы диалогового окна */
  }


  fieldsCategories = [
    "title"
  ];


  rowActions = [
    // { name: 'edit',   handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    // { name: 'delete', handle: (actionName, item) => this.handleDialogOpen(actionName, item) },
    { name: 'download', handle: (actionName, item) => {
        return item.fileUrl;
      }
    },
  ];


  componentWillMount() {
    const { documentTitle } = this.state;
    const { itemId } = this.props.match.params;
    document.title = documentTitle;
    this.tracker = Tracker.autorun(() => {
      const currentCategory = ProjectCategories.findOne({
        'projects': {
          $elemMatch: {
            shortId: itemId
          }
        }
      });
      this.setState({ currentCategory: currentCategory });
    });
  }


  // prepareData = category => {
  //   category.competitions = category.competitions.map(({ applications, ...other}) => ({
  //     ...other,
  //     applications: !applications ? [] : applications.map(({ fileUrl, ...other }) => ({
  //       fileUrl: fileUrl ? `<a href="${fileUrl}">СКАЧАТЬ</a>` : '',
  //       ...other
  //     }))
  //   }))
  //   return category;
  // }


  getProjectIndex = shortId => {
    const { currentCategory: { projects } } = this.state;
    return projects.findIndex(i => i.shortId === shortId);
  }


  getProject = () => {
    const { currentCategory: { projects } } = this.state;
    const { itemId } = this.props.match.params;
    const index = this.getProjectIndex(itemId);
    return projects[index];
  }


  handleDialogOpen = (type, itemsId = null) => {
    this.setState({ dialogTypeOpened: type });
    this.setState({ dialogOpened: true });
    this.setState({ dialogSelected: itemsId });
  };


  handleDialogClose = () => {
    this.setState({ dialogOpened: false });
    this.setState({ selected: [] });
  };


  renderDialogContent = () => {
    const { dialogTypeOpened, currentCategory } = this.state;
    const { itemId } = this.props.match.params;
    const index = this.getProjectIndex(itemId);
    // if (!dialogTypeOpened || dialogTypeOpened === 'add') return <AddCompetition handleClose={this.handleDialogClose} category={currentCategory}/>;
    // // if (dialogTypeOpened === 'show') return <AddCompetition handleClose={this.handleDialogClose} item={dialogSelected} />;
    // if (dialogTypeOpened === 'edit') return <AddCompetition handleClose={this.handleDialogClose} item={dialogSelected} category={currentCategory} competitionIndex={this.getCompetiotionIndex(dialogSelected)}/>;
    // if (dialogTypeOpened === 'delete') return <DeleteCompetition handleClose={this.handleDialogClose} items={dialogSelected} category={currentCategory}/>
    if (dialogTypeOpened === 'chart') return <ProjectApplicationsChart handleClose={this.handleDialogClose} category={currentCategory} projectIndex={index}/>
    return '';
  }


  render() {
    const { dialogOpened, documentTitle, currentCategory } = this.state;
    const project = this.getProject();
    const applications = project.applications ? project.applications : [];
    const backButtonLink = '/project/' + currentCategory.shortId;

    return (
      <SidebarWrapper 
        title={ documentTitle }>
          <PaginationTable 
            data={ applications } 
            fields={ this.fieldsCategories }
            handleDialogOpen={ this.handleDialogOpen }
            colHeadersCustom={ colHeaders }
            rowActions={ this.rowActions }
            tableTitle="Список заявок"
            backButtonLink={ backButtonLink }/>
          <DialogWindow
            open={ dialogOpened }
            handleClose={ this.handleDialogClose }>
              { this.renderDialogContent() }
          </DialogWindow>
      </SidebarWrapper>
    )
  }
}

ProjectApplicationsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  competition: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(ProjectApplicationsSection))