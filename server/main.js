import { Meteor } from 'meteor/meteor';
import '/imports/api/publications-user';

import CreateDefaultAdmin from './migrations/18-11-18-create-admin';

import { CompetitionCategories } from '../imports/api/competition-categories';
import { News } from '../imports/api/news';
import { MethodSpaceCategories } from '/imports/api/method-space-categories';
import { ProjectCategories } from '../imports/api/project-categories';

Meteor.startup(() => {
  CreateDefaultAdmin();
});