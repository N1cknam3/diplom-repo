import { Meteor } from 'meteor/meteor';
import ServerSideConstants from '/imports/api/server-side-constants';
import { Roles } from 'meteor/alanning:roles';

/*
* Добавление дефолтных администраторов
* */
export default () => {
  let defaultUsersData = ServerSideConstants.AutoUsers;
  let defaultUsersEmails = defaultUsersData.map(item => item.email);
  const query = {'emails.address': {$in: defaultUsersEmails}};
  const existedUsersEmails = Meteor.users.find(query).map(item => item.emails[0].address);
  if (existedUsersEmails.length === defaultUsersEmails.length) return; // Все дефолтные пользователи созданы
  console.log('migrations/18-11-18-create-admin - started');
  const neededUsers = defaultUsersData.filter((userData) => !existedUsersEmails.includes(userData.email));
  neededUsers.map((userData) => {
    const createdUserId = Accounts.createUser({ email: userData.email, password: userData.password });
    if (createdUserId && userData.roles) Roles.addUsersToRoles(createdUserId, userData.roles);
  });
  console.log('migrations/18-11-18-create-admin - finished');
}