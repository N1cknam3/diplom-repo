import React from 'react';
import { Meteor } from 'meteor/meteor';
import { BrowserRouter as Router, Redirect, Route, Switch, IndexRoute } from 'react-router-dom';
import { Roles } from 'meteor/alanning:roles';

import Admin from '/imports/ui/admin/admin';
import CompetitionApplicationsSection from '/imports/ui/admin/competition-applications/competition-applications';
import CompetitionSection from '/imports/ui/admin/competition/competition-section';
import CompetitionsSection from '/imports/ui/admin/competitions/competitions-section';
import Login from '/imports/ui/login/login';
import Methodist from '/imports/ui/methodist/methodist';
import NewsSection from '/imports/ui/admin/news/news-section';
import MethodSpaceSection from '/imports/ui/admin/method-space/method-space-section';
import MethodSpacesSection from '/imports/ui/admin/method-spaces/method-spaces-section';
import PrivateRoutes from '/imports/ui/helpers/privateRoutes';
import ProjectApplicationsSection from '/imports/ui/admin/project-applications/project-applications';
import ProjectSection from '/imports/ui/admin/project/project-section';
import ProjectsSection from '/imports/ui/admin/projects/projects-section';

export default class AppRouter extends Router {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/login"/>}/>
          <Route exact path="/login" render={() => {
            if (Meteor.userId())
              return <PrivateRoutes toRenderAdmin={() => <Redirect to="/admin"/>} toRenderMethodist={() => <Redirect to="/methodist"/>}/>;
            return <Login/>
          }}/>
          <Route exact path="/logout" render={() => {
            Meteor.logout((err) => {
              if (err) return console.log(err);
              setTimeout(() => {window.location = Meteor.absoluteUrl()}, 100);
            });
            return <Login/>
          }}/>
          <Route exact path="/methodist" render={() => {
            return <Redirect to="/methodist/news"/>;
          }}/>
          <Route path="/methodist/:path" render={() =>
            <PrivateRoutes role="methodist" toRender={() => <Methodist/>}/>
          }/>
          <Route exact path="/admin" render={() =>
            <PrivateRoutes role="admin" toRender={() => <Admin/>}/>
          }/>
          <Route exact path="/news" render={() =>
            <PrivateRoutes role="admin" toRender={() => <NewsSection/>}/>
          }/>
          <Route exact path="/competitions" render={() =>
            <PrivateRoutes role="admin" toRender={() => <CompetitionsSection/>}/>
          }/>
          <Route exact path="/competition/:categoryId" render={() =>
            <PrivateRoutes role="admin" toRender={() => <CompetitionSection/>}/>
          }/>
          <Route exact path="/competition-applications/:itemId" render={() =>
            <PrivateRoutes role="admin" toRender={() => <CompetitionApplicationsSection/>}/>
          }/>
          <Route exact path="/projects" render={() =>
            <PrivateRoutes role="admin" toRender={() => <ProjectsSection/>}/>
          }/>
          <Route exact path="/project/:categoryId" render={() =>
            <PrivateRoutes role="admin" toRender={() => <ProjectSection/>}/>
          }/>
          <Route exact path="/project-applications/:itemId" render={() =>
            <PrivateRoutes role="admin" toRender={() => <ProjectApplicationsSection/>}/>
          }/>
          <Route exact path="/method-spaces/" render={() =>
            <PrivateRoutes role="admin" toRender={() => <MethodSpacesSection/>}/>
          }/>
          <Route exact path="/method-space/:categoryId" render={() =>
            <PrivateRoutes role="admin" toRender={() => <MethodSpaceSection/>}/>
          }/>
          <Redirect to="/login"/>
        </Switch>
      </Router>
    )
  }
}