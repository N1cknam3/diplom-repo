import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { AddNews } from '/imports/ui/admin/news/add';
import { configure, mount, shallow } from 'enzyme';

configure({ adapter: new Adapter() });

global.URL.createObjectURL = jest.fn().mockImplementation(file => "www.test.com");

describe('AddNews', () => {

  describe('Add new news item form', () => {
    const initProps = {
      history: [],
      classes: {},  // Имитация подключения классов через withStyles
      theme: {},
      handleClose: jest.fn(),
      dataSource: [],
    }

    const addNews = mount(<AddNews {...initProps}/>);

    afterAll(() => {
      addNews.unmount();
    });

    describe('when submit a blank form', () => {
      beforeEach(() => {
        const event = {
          preventDefault: () => {}
        }
        addNews.find('form').simulate('submit', event);
      })

      it('closing handler havent been fired', () => {
        expect(addNews.props().handleClose).toHaveBeenCalledTimes(0)
      })
    })

    describe('when submitting the blank form', () => {
      beforeEach(() => {
        const event = {
          preventDefault: () => {}
        }
        addNews.find('form').simulate('submit', event);
      })

      it('form submit button disabled', () => {
        expect(addNews.state().formValid).toBeFalsy();
      })
    })

    describe('when entering an empty title text', () => {
      const title = "   ";

      beforeEach(() => {
        const input = addNews.find('textarea#title');
        const event = {
          target: {
            name: 'title',
            value: title,
          }
        }
        input.simulate('change', event);
      })

      it('form is invalid', () => {
        expect(addNews.state().formValid).toBeFalsy();
      })
    })

    describe('when entering an empty shortId text', () => {
      const shortId = "   ";

      beforeEach(() => {
        const input = addNews.find('textarea#shortId');
        const event = {
          target: {
            name: 'shortId',
            value: shortId,
          }
        }
        input.simulate('change', event);
      })

      it('form is invalid', () => {
        expect(addNews.state().formValid).toBeFalsy();
      })
    })

    describe('when entering a title text', () => {
      const title = "test title";

      beforeEach(() => {
        const input = addNews.find('textarea#title');
        const event = {
          target: {
            name: 'title',
            value: title,
          }
        }
        input.simulate('change', event);
      })

      it('form is valid', () => {
        expect(addNews.state().formValid).toBeTruthy();
      })
    })

    describe('when changing a shortId text', () => {
      const shortId = "test title shortId";

      beforeEach(() => {
        const input = addNews.find('textarea#shortId');
        const event = {
          target: {
            name: 'shortId',
            value: shortId,
          }
        }
        input.simulate('change', event);
      })

      it('form is valid', () => {
        expect(addNews.state().formValid).toBeTruthy();
      })
    })

    describe('when entering a long title text', () => {
      const title = "test title with long long long text";

      beforeEach(() => {
        const input = addNews.find('textarea#title');
        const event = {
          target: {
            name: 'title',
            value: title,
          }
        }
        input.simulate('change', event);
      })

      it('we limit shortId with 5 words', () => {
        expect(addNews.state().item.shortId).toEqual('test-title-with-long-long');
      })
    })

    describe('when success submit a form without file', () => {
      beforeEach(() => {
        const event = {
          preventDefault: () => {}
        }
        addNews.find('form').simulate('submit', event);
      })

      it('form closing handler have been fired', () => {
        expect(addNews.props().handleClose).toHaveBeenCalledTimes(1)
      })
    })
    
  })

  describe('Edit news item form', () => {
    const initProps = {
      history: [],
      classes: {},  // Имитация подключения классов через withStyles
      theme: {},
      handleClose: jest.fn(),
      dataSource: [],
    }
    
    const item = {
      _id: 'testID',
      title: 'News item to edit',
      text: 'Description test',
      shortId: 'news-item-to-edit',
      createdAt: new Date(),
      imageUrl: '',
      fileName: '',
    }
    const editNews = mount(<AddNews {...initProps} item={item}/>);

    afterAll(() => {
      editNews.unmount();
    });

    describe('when open a form with item to edit', () => {
      it('we can see title in the title field', () => {
        const input = editNews.find('textarea#title');
        expect(input.props().value).toEqual(item.title);
      })
    })

    describe('when submit a form with initial item without a file', () => {
      beforeEach(() => {
        const event = {
          preventDefault: () => {}
        }
        editNews.find('form').simulate('submit', event);
      })

      it('form closing handler have been fired', () => {
        expect(editNews.props().handleClose).toHaveBeenCalledTimes(1)
      })
    })

    describe('when selected a file', () => {
      const file = {
        name: 'selected_file_name.jpg'
      }

      beforeEach(() => {
        editNews.find('ImageUpload').prop('onSelected')(file)
      })

      it('it shows in state', () => {
        expect(editNews.state().selectedFile).toEqual(file)
      })
    })

    describe('when remove selected file', () => {
      beforeEach(() => {
        editNews.find('ImageUpload').prop('onRemove')()
      })

      it('it removes in state', () => {
        expect(editNews.state().selectedFile).toBeNull()
      })
    })

    // describe('when select a file and upload to server', () => {
    //   const file = {
    //     name: 'selected_file_name.jpg'
    //   }

    //   beforeEach(() => {
    //     const event = {
    //       preventDefault: () => {}
    //     }
    //     editNews.find('ImageUpload').prop('onSelected')(file);
    //     editNews.find('form').simulate('submit', event);
    //   })

    //   it('form closing handler have been fired', () => {
    //     expect(editNews.state().selectedFile).toEqual(file)
    //   })
    // })
  })

})