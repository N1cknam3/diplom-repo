let usersQueryResult = [];

export function __setUsersQueryResult(result) {
  usersQueryResult = result;
}

export function translit(text) {
  return text.split(' ').join('-');
}

// export function moment(date) {
//   const _date = date;
//   this.format = formatString => {
//     return _date;
//   }
//   this.locale = locale => {
//     return;
//   }
// };

export const Meteor = {
  users: {
    findOne: jest.fn().mockImplementation(() => usersQueryResult),
    find: jest.fn().mockImplementation(() => ({
      fetch: jest.fn().mockReturnValue(usersQueryResult),
      count: jest.fn(),
    })),
  },
  loginWithPassword: jest.fn().mockImplementation((email, password, callback = () => {}) => {
    if (email && password) callback(false);
    else callback(true);
  }),
};

export const Mongo = {
  Collection: jest.fn().mockImplementation(() => ({
    _ensureIndex: (jest.fn()),
    insert: jest.fn(),
    update: jest.fn(),
  })),
};

