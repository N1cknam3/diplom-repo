import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { Login } from '/imports/ui/login/login';
import { configure, mount, shallow } from 'enzyme';

configure({ adapter: new Adapter() });

describe('Login', () => {
  const initProps = {
    history: [],
    classes: {},  // Имитация подключения классов через withStyles
  }
  const login = mount(<Login {...initProps}/>);

  describe('Form handlers', () => {

    describe('when submitting the blank form', () => {
      beforeEach(() => {
        const event = {
          preventDefault: () => {}
        }
        login.find('form').simulate('submit', event);
      })

      it('opens error snackbar', () => {
        expect(login.state().snackBar.open).toBeTruthy();
        expect(login.state().snackBar.variant).toBe('error');
      })
    })

    describe('when click outside error snackbar', () => {
      beforeEach(() => {
        login.find('AppSnackBar').prop('onClose')({}, 'clickaway')
      })

      it('snackbar not hides', () => {
        expect(login.state().snackBar.open).toBeTruthy();
        expect(login.state().snackBar.variant).toEqual('error');
      })
    })

    describe('when closing error snackbar', () => {
      beforeEach(() => {
        login.find('AppSnackBar').prop('onClose')({})
      })

      it('snackbar hides', () => {
        expect(login.state().snackBar.open).toBeFalsy();
        expect(login.state().snackBar.variant).not.toEqual('error');
      })
    })

    describe('when typing into email input', () => {
      const email = "test@test.com";

      beforeEach(() => {
        const input = login.find('Input#email').find('input');
        const event = {
          target: {
            name: 'email',
            value: email,
          }
        }
        input.simulate('change', event);
      })

      it('updates email field in state', () => {
        expect(login.state().email).toEqual(email);
      })
    })

    describe('when typing into password input', () => {
      const password = "123";

      beforeEach(() => {
        const input = login.find('Input#password').find('input');
        const event = {
          target: {
            name: 'password',
            value: password,
          }
        }
        input.simulate('change', event);
      })

      it('updates password field in state', () => {
        expect(login.state().password).toEqual(password);
      })
    })

    describe('when submitting the form', () => {
      beforeEach(() => {
        const event = {
          preventDefault: () => {}
        }
        login.find('form').simulate('submit', event);
      })

      it('redirect to the cabinet page', () => {
        expect(login.props().history).toHaveLength(1);
      })
    })

  })

})